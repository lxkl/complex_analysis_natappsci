# Complex Analysis for Natural and Applied Sciences

Copyright 2019, 2020 Lasse Kliemann <l.kliemann@math.uni-kiel.de>

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
