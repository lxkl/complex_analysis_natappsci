\chapter{Residue Theorem}
\label{cha:cha070}

\section{The Theorem}

\begin{para}
  Let $\Om \subseteq \CC$ be open, $z_0 \in \Om$, $\Om_{0} \df \Om \setminus \set{z_{0}}$,
  and let ${f \in \fnset{\Om_{0}}{\CC}}$ be holomorphic.
  Let $r > 0$ such that $\clD(z_0,r) \subseteq \Om$.
  Then we call
  \begin{IEEEeqnarray*}{0l}
    \res(f,z_0) \df \frac{1}{2\pi\imagi} \int_{\kappa(z_0,r)} f(\zeta) \de \zeta
  \end{IEEEeqnarray*}
  the \term{residue} of $f$ in $z_0$.
  By \autoref{exc:cha050:circles-equal}, this is independent of the actual choice of~$r$,
  so $\res(f,z_0)$ indeed only depends on $f$ and on $z_0$.
  By \autoref{prop:cha060:laurent}, if $\seqint{a_n}{n}$ are the Laurent coefficients of $f$ around $z_0$,
  then $\res(f,z_0) = a_{-1}$.
  Given the Laurent series,
  the latter can also be seen directly by integrating the Laurent series of $f$ around $z_{0}$.
  By \autoref{prop:cha060:unif-int}, integration and summation may be swapped,
  and of all the integrals under the sum, only that for $n=-1$ remains due to \autoref{prop:cha030:2pii};
  it is the \enquote{residue} of integrating the Laurent series.
\end{para}

\begin{para}
  Let $\Om \subseteq \CC$ be open, $S \subseteq \Om$ finite, and $\Om_{S} \df \Om \setminus S$.
  Then $\Om_{S}$ is open.
  If $f \in \fnset{\Om_{S}}{\CC}$ is holomorphic,
  then $S$ consists only of isolated singularities of~$f$,
  and for all $z \in S$, we have a residue $\res(f,z)$.
\end{para}

\begin{theorem}[Residue Theorem]
  Let $\Om \subseteq \CC$ be a domain (that is, open and connected),
  $S \subseteq \Om$ finite, and $\Om_{S} \df \Om \setminus S$.
  Let $f \in \fnset{\Om_{S}}{\CC}$ be holomorphic.
  Let $\Gam$ be a cycle in $\Om_{S}$ which is NH in~$\Om$;
  note that being NH in $\Om$ is a weaker condition than being NH in~$\Om_{S}$.
  Then:
  \begin{IEEEeqnarray*}{0l}
    \int_\Gam f(\zeta) \de \zeta = 2\pi\imagi \sum_{z \in S} \res(f,z) \ccdot \ind_\Gam(z)
  \end{IEEEeqnarray*}
\end{theorem}

\begin{proof}
  Write $S = \set{\eli{z}{n}}$.
  For all $z_j$ let $r_j > 0$ such that $\clD(z_{j},r_{j}) \subseteq \Om_{S} \cup \set{z_j}$,
  and define $\kappa_j \df \kappa(z_j, r_j)$.
  Hence $\kappa_j$ runs in the domain of $f$
  and $\ind_{\kappa_j}(z_j) = 1$ and $\ind_{\kappa_j}(z_l) = 0$ for all $l \neq j$.
  Let $\Gam'$ be that cycle which for all $j \in \setn{n}$
  runs along $-\kappa_j$ exactly $\ind_\Gam(z_j)$ times, that is (recall \autoref{para:cha050:n-gam}):
  \begin{IEEEeqnarray*}{0l}
    \Gam' \df \set{-\ind_\Gam(z_1) \kappa_{1}, \hdots, -\ind_\Gam(z_n) \kappa_{n}}\isbag
  \end{IEEEeqnarray*}
  Define $\tiGam \df \Gam \cup \Gam'$.
  Then $\tiGam$ is NH in $\Om_{S}$. It follows with \CIFxx:
  \begin{IEEEeqnarray*}{0l+x*}
    0 = \int_{\tiGam} f(\zeta) \de \zeta
    = \int_{\Gam} f(\zeta) \de \zeta
    + \sum_{\gam \in \Gam'} \int_{\gam} f(\zeta) \de \zeta \\
    \quad = \int_{\Gam} f(\zeta) \de \zeta
    - \sum_{j=1}^{n} \ind_{\Gam}(z_{j}) \int_{\kappa_{j}} f(\zeta) \de \zeta \\
    \quad = \int_{\Gam} f(\zeta) \de \zeta
    - \sum_{j=1}^{n} \ind_{\Gam}(z_{j}) \ccdot 2\pi\imagi \ccdot \res(f,z_{j}) & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}
  \emphasis{The Residue Theorem reduces the computation of curve integrals
    to the computation of residues and winding numbers}.
  For winding numbers, we have the crossing lemma, which is often sufficient.
  For residues, in case of poles, the following proposition is often helpful.
\end{para}

\begin{proposition}
  \label{prop:cha070:pole-diff}
  Let $\Om \subseteq \CC$ be open, $z_0 \in \Om$, $\Om_{0} \df \Om \setminus \set{z_{0}}$,
  and let ${f \in \fnset{\Om_{0}}{\CC}}$ be holomorphic.
  Let $z_0$ be a pole of $f$ of order at most $p \in \NN$, and let $G \in \fnset{\Om}{\CC}$ be
  the holomorphic extension of $g \df \fn{z}{\Om_{0}}{(z-z_0)^p \ccdot f(z)}$,
  which exists by \autoref{prop:cha060:pole-order} (even if $p$ is more than the order of the pole);
  see also the proof of \autoref{prop:cha060:split-pole}.
  Then:
  \begin{IEEEeqnarray*}{0l}
    \res(f,z_0) = \frac{\diff{G}{p-1}(z_0)}{(p-1)!} \\
    \res(f,z_0) = \flim{z}{z_0} \frac{\diff{g}{p-1}(z)}{(p-1)!}
    = \frac{\flim{z}{z_0} \diff{g}{p-1}(z)}{(p-1)!}
  \end{IEEEeqnarray*}
\end{proposition}

\begin{proof}
  Let $f(z) = \sum_{n=-q}^\infty a_n \ccdot (z-z_0)^n$ be the Laurent representation of $f$ around~$z_0$,
  with $1 \leq q \leq p$ being the order of the pole.
  Then for all $z$ in a neighborhood of $z_{0}$:
  \begin{IEEEeqnarray*}{0l}
    G(z)
    = \sum_{n=-q}^\infty a_n (z-z_0)^{n+p}
    = \sum_{n=p-q}^\infty a_{n-p} (z-z_0)^n
  \end{IEEEeqnarray*}
  Denote $\seqzero{b_{n}}{n}$ the Taylor coefficients of $G$.
  Then $b_{n} = 0$ for all $0 \leq n < p-q$
  and $b_{n} = a_{n-p}$ for all $n \geq p-q$.
  Since $b_{n} = \taylor{G}{n}{z_0}$ for all $n$,
  in particular:
  \begin{IEEEeqnarray*}{0l}
    \taylorp{G}{p-1}{z_0} = b_{p-1} = a_{(p-1)-p} = a_{-1} = \res(f,z_0)
  \end{IEEEeqnarray*}
  The additional statement follows from this
  since $\diff{G}{p-1}$ is continuous and coincides on $\Om_{0}$ with $\diff{g}{p-1}$.
\end{proof}

\begin{para}
  The preceding proposition can be summarized as follows.
  First, we remove the pole by multiplying with $(z-z_{0})^{p}$,
  which can be thought of as a dampening or cooling factor.
  Then we compute the $(p-1)$th derivative and determine its limit for $z \tends z_{0}$.
  Finally, this limit is divided by $(p-1)!$, and the result is the residue.
  The number $p$ can be any integer at least as large as the order of the pole,
  but in practice it may make sense to not choose $p$ unnecessarily high,
  since the higher $p$ is, the higher derivatives have to be computed.
\end{para}

\begin{proposition}
  \label{prop:cha070:sing-diff}
  Let $\Om \subseteq \CC$ be open, and let ${f,g \in \fnset{\Om}{\CC}}$ be holomorphic.
  Let $z_{0} \in \Om$, and assume $\ord(f,z_0) = 0$ and  $\ord(g,z_0) = 1$.
  Let $N$ be a neighborhood of $z_0$ on which $g$ has no zero except for $z_0$.
  Define $h \df \fn{z}{N_{0}}{\frac{f(z)}{g(z)}}$, where $N_{0} \df N \setminus \set{z_{0}}$.
  Then:
  \begin{IEEEeqnarray*}{0l}
    \res(h, z_0) = \frac{f(z_0)}{g'(z_0)}
  \end{IEEEeqnarray*}
\end{proposition}

\begin{proof}
  The function $h$ has in $z_0$ a pole of order~$1$.
  By \autoref{prop:cha070:pole-diff}, we have:
  \begin{IEEEeqnarray*}{0l+x*}
    \res(h, z_0)
    = \lim_{z \tends z_0} (z-z_0) \ccdot h(z)
    = \lim_{z \tends z_0} \frac{f(z)}{\frac{g(z)}{z-z_0}}
    = \lim_{z \tends z_0} \frac{f(z)}{\frac{g(z)-g(z_0)}{z-z_0}}
    = \frac{f(z_0)}{g'(z_0)} & \qedarray[\qedhere]{1}%
  \end{IEEEeqnarray*}
\end{proof}

\section{First Applications to Improper Integrals}

\begin{example}
  Let $a,b > 0$. We like to compute:
  \begin{IEEEeqnarray*}{0l}
    \int_{\ninfty}^\pinfty \frac{\cos(a x)}{x^2 + b^2} \de x
    = \int_{\ninfty}^\pinfty \frac{\cos(a x)}{(x+\imagi b) (x-\imagi b)} \de x
  \end{IEEEeqnarray*}
  First, we show that this integral converges.
  We have $\abs{\frac{\cos(a x)}{x^2 + b^2}} \leq \frac{1}{x^2}$ for all $x \in \RRnz$.
  Convergence follows from the comparison criterion and \autoref{prop:cha030:improper-alpha}.
  Due to convergence, we have:
  \begin{IEEEeqnarray*}{0l}
    \int_{\ninfty}^\pinfty \frac{\cos(a x)}{x^2 + b^2} \de x
    = \flim{R}{\pinfty} \int_{-R}^R \frac{\cos(a x)}{x^2 + b^2} \de x
  \end{IEEEeqnarray*}
  Recall that before we have established convergence, we may not use this formula.
  \par
  Now define:
  \begin{IEEEeqnarray*}{0l}
    f \df \fn{z}{\CC\setminus\set{\pm\imagi b}}{\frac{e^{\imagi a z}}{(z+\imagi b) (z-\imagi b)}}
  \end{IEEEeqnarray*}
  Let $R > b$, and define $\al(R) \df \tau(-R, R)$
  and $\beta(R) \df \fnres{\kappa(0,R)}{\intcc{0}{\pi}}$.
  Then $\al(R)$ runs along the real axis from $-R$ to $R$,
  and $\beta(R)$ runs back from $R$ to $-R$ along a semicircle.
  The curve $\gam(R) \df \al(R) \cat \beta(R)$ is closed and clearly NH in $\CC$.
  By the crossing lemma, $\ind_{\gam(R)}(-\imagi b) = 0$ and $\ind_{\gam(R)}(\imagi b) = 1$.
  By the Residue Theorem:
  \begin{IEEEeqnarray*}{0l}
    \int_{\gam(R)} f(z) \de z = 2\pi\imagi \ccdot \res(f,\imagi b)
  \end{IEEEeqnarray*}
  We have $\ord(\fn{z}{\CC}{e^{\imagi a z}}, \imagi b) = 0$ since the exponential function is never~$0$.
  The denominator of $f$ has order~$1$ in $\imagi b$.
  Hence $f$ has a pole of order~$1$ in $\imagi b$,
  so we consider the $0$th derivative of
  $\fn{z}{\CC\setminus\set{\pm\imagi b}}{(z-\imagi b)^1 \ccdot f(z)}$,
  that is, this function itself. We have:
  \begin{IEEEeqnarray*}{0l}
    \res(f,\imagi b) = \flim{z}{\imagi b} (z-\imagi b) \ccdot f(z)
    = \flim{z}{\imagi b} \frac{e^{\imagi a z}}{z+\imagi b}
    = \frac{e^{\imagi a \imagi b}}{\imagi b+\imagi b} = \frac{e^{-ab}}{2\imagi b}
  \end{IEEEeqnarray*}
  What happens here is that the zero of the denominator simply cancels out,
  yielding a function that can easily be extended into $\imagi b$ in a holomorphic manner,
  namely we have the function $\fn{z}{\CC\setminus\set{-\imagi b}}{\frac{e^{\imagi a z}}{z+\imagi b}}$.
  Then this function is evaluated in $\imagi b$.
  \par
  Now we know:
  \begin{IEEEeqnarray*}{0l}
    \int_{-R}^R \frac{\cos(a x)}{x^2 + b^2} \de x
    = \Re \int_{\gam(R)} f(z) \de z - \Re \int_{\beta(R)} f(z) \de z \\
    \quad = \Re\parens{2\pi\imagi \frac{e^{-ab}}{2\imagi b}} - \Re \int_{\beta(R)} f(z) \de z
    = \frac{\pi e^{-ab}}{b} - \Re \int_{\beta(R)} f(z) \de z
  \end{IEEEeqnarray*}
  We show that the last term tends to~$0$ when $R \tends \pinfty$, then we obtain our integral, namely:
  \begin{IEEEeqnarray*}{0l}
    \int_{-R}^R \frac{\cos(a x)}{x^2 + b^2} \de x = \frac{\pi e^{-ab}}{b}
  \end{IEEEeqnarray*}
  For all $z \in \img(\beta(R))$, using the triangle inequality for differences:
  \begin{IEEEeqnarray*}{0l}
    \frac{\abs{e^{\imagi a z}}}{\abs{z^2 + b^2}}
    = \frac{e^{\Re(\imagi a z)}}{\abs{z^2 + b^2}}
    = \frac{e^{- a \ccdot \Im(z)}}{\abs{z^2 + b^2}}
    \leq \frac{1}{\abs{z^2 + b^2}}
    \leq \frac{1}{\abs{\abs{z^2} - \abs{b^2}}}
    \leq \frac{1}{R^2 - b^2}
  \end{IEEEeqnarray*}
  It follows:
  \begin{IEEEeqnarray*}{0l}
    \abs{ \int_{\beta(R)} f(z) \de z }
    \leq L(\beta(R)) \max_{z \in \img(\beta(R))} \frac{\abs{e^{\imagi a z}}}{\abs{z^2 + b^2}}
    = \pi R \frac{1}{R^2 - b^2}
    \tendsasp{R} 0
  \end{IEEEeqnarray*}
\end{example}
\smallskip

\begin{example}
  \label{ex:cha070:x2+1}
  We like to compute:
  \begin{IEEEeqnarray*}{0l}
    \int_{\ninfty}^{\pinfty} \frac{1}{(x^2+1)^2} \de x
    = \int_{\ninfty}^{\pinfty} \frac{1}{(x+\imagi)^2 (x-\imagi)^2} \de x
  \end{IEEEeqnarray*}
  Convergence again follows from the comparison criterion and \autoref{prop:cha030:improper-alpha}.
  Define:
  \begin{IEEEeqnarray*}{0l}
    f \df \fn{z}{\CC\setminus\set{\pm\imagi}}{\frac{1}{(z+\imagi)^2 (z-\imagi)^2}}
  \end{IEEEeqnarray*}
  We use $\gam(R)$ from the previous example.
  By the crossing lemma, ${\ind_{\gam(R)}(-b) = 0}$ and ${\ind_{\gam(R)}(\imagi b) = 1}$.
  The function $f$ has a pole of order~$2$ in $\imagi$,
  and by \autoref{prop:cha070:pole-diff} (using short notation for functions):
  \begin{IEEEeqnarray*}{0l}
    \res(f,\imagi)
    = \frac{\de}{\de z} \parens{z \mapsto \frac{1}{(z+\imagi)^2}} (\imagi)
    = \parens{z \mapsto \frac{-2}{(z+\imagi)^3}} (\imagi)
    = \frac{1}{4 \imagi}
  \end{IEEEeqnarray*}
  We demonstrate what happens when we choose $p$ in \autoref{prop:cha070:pole-diff}
  higher than the order of the pole. Say, we choose $p=3$ here. Then we obtain:
  \begin{IEEEeqnarray*}{0l}
    \frac{1}{2!} \frac{\de^{2}}{\de z^{2}} \parens{z \mapsto \frac{z-\imagi}{(z+\imagi)^2}} (\imagi)
    = \frac{1}{2!} \frac{\de}{\de z}
    \parens{z \mapsto \frac{3\imagi - z}{(z+\imagi)^3}} (\imagi) \\
    \quad = \frac{1}{2!} \parens{z \mapsto \frac{-10 \imagi + 2 z}{(z+\imagi)^4}} (\imagi)
    = \frac{1}{2!} \ccdot \frac{-8\imagi}{16}
    = \frac{1}{2!} \ccdot \frac{8}{16 \imagi}
    = \frac{1}{4 \imagi}
  \end{IEEEeqnarray*}
  The computation is longer, but the result is the same.
  \par
  Now we know:
  \begin{IEEEeqnarray*}{0l}
    \int_{-R}^R \frac{1}{(x^2+1)^2} \de x
    = \Re \int_{\gam(R)} f(z) \de z - \Re \int_{\beta(R)} f(z) \de z \\
    \quad = \Re\parens{2\pi\imagi \frac{1}{4 \imagi}} - \Re \int_{\beta(R)} f(z) \de z
    = \frac{\pi}{2} - \Re \int_{\beta(R)} f(z) \de z
  \end{IEEEeqnarray*}
  Again, the contribution of the semicircle vanishes for large $R$:
  \begin{IEEEeqnarray*}{0l}
    \abs{ \int_{\beta(R)} f(z) \de z }
    \leq \pi R \frac{1}{(R^2-1)^2}
    \tendsasp{R} 0
  \end{IEEEeqnarray*}
  We obtain $\frac{\pi}{2}$ as the value of the integral.
\end{example}
\smallskip

\begin{example}
  We like to compute:
  \begin{IEEEeqnarray*}{0l}
    \int_{\ninfty}^{\pinfty} \frac{1}{(x^2+1)^3} \de x
    = \int_{\ninfty}^{\pinfty} \frac{1}{(x+\imagi)^3 (x-\imagi)^3} \de x
  \end{IEEEeqnarray*}
  Most steps are just like in the preceding example.
  We only look at the part where the Residue Theorem is involved.
  Define:
  \begin{IEEEeqnarray*}{0l}
    f \df \fn{z}{\CC\setminus\set{\pm\imagi}}{\frac{1}{(z+\imagi)^3 (z-\imagi)^3}}
  \end{IEEEeqnarray*}
  Then $f$ has a pole of order~$3$ in $\imagi$ and:
  \begin{IEEEeqnarray*}{0l}
    \res(f,\imagi)
    = \frac{1}{2!} \frac{\de^2}{\de z^2} \parens{z \mapsto \frac{1}{(z+\imagi)^3}} (\imagi)
    = \frac{1}{2} \parens{z \mapsto \frac{(-3) \ccdot (-4)}{(z+\imagi)^5}} (\imagi)
    = \frac{3}{16 \imagi}
  \end{IEEEeqnarray*}
  It follows:
  \begin{IEEEeqnarray*}{0l}
    \int_{\ninfty}^{\pinfty} \frac{1}{(x^2+1)^3} \de x = \frac{3 \pi}{8}
  \end{IEEEeqnarray*}
\end{example}
\smallskip

\begin{example}
  Let $0 < a < 1$. We like to compute:
  \begin{IEEEeqnarray*}{0l}
    \int_{\ninfty}^{\pinfty} \frac{e^{ax}}{1+e^x} \de x
  \end{IEEEeqnarray*}
  Since the integrand cannot be negative, this integral converges if and only if
  $\flim{R}{\pinfty} \int_{-R}^R \frac{e^{ax}}{1+e^x} \de x \in \RR$.
  \par
  Let $R > 0$.
  Define $\gam = (\gam_1, \gam_2, \gam_3, \gam_4)$ with:
  \begin{IEEEeqnarray*}{0l+l}
    \gam_1 \df \tau(-R, \, R)
    &\gam_2 \df \tau(R, \, R + 2 \pi \imagi) \\
    \gam_3 \df \tau(R + 2 \pi \imagi, \, -R + 2 \pi \imagi)
    &\gam_4 \df \tau(-R + 2 \pi \imagi, \, -R)
  \end{IEEEeqnarray*}
  So $\gam$ runs along the boundary of a rectangle that grows in horizontal direction when we increase $R$.
  The interesting part is of course $\gam_{1}$, which runs along the real axis.
  Define:
  \begin{IEEEeqnarray*}{0l}
    f \df \fn{z}{\CC \setminus \set{(2m+1) \pi\imagi \suchthat m \in \ZZ}}{\frac{e^{az}}{1+e^z}}
  \end{IEEEeqnarray*}
  The only relevant singularity is $\pi\imagi$.
  We have $\ord(z\mapsto 1+e^z, \, \pi\imagi) = 1$ and since the numerator of $f$ is never~$0$,
  we have a pole of order~$1$ in $\pi\imagi$.
  By \autoref{prop:cha070:sing-diff}:
  \begin{IEEEeqnarray*}{0l}
    \res(f, \pi\imagi) = \frac{e^{a \pi \imagi}}{e^{\pi\imagi}} = - e^{a \pi \imagi}
  \end{IEEEeqnarray*}
  It follows $\int_\gam f(z) \de z = - 2 \pi \imagi \ccdot e^{a \pi \imagi}$ from the Residue Theorem.
  This cannot be the value of the original integral, since that one is real.
  Hence in contrast to all the previous examples,
  here must be a more complex interaction between the integrals along the different parts of the closed curve.
  We look at the vertical parts of the rectangle first,
  using the triangle inequality for differences:
  \begin{IEEEeqnarray*}{0l}
    \abs{\int_{\gam_2} f(z) \de z}
    \leq L(\gam_2) \max_{z \in \img(\gam_2)} \frac{e^{a \ccdot \Re(z)}}{\abs{1+e^z}}
    = 2 \pi \max_{t \in \intcc{0}{2\pi}} \frac{e^{a R}}{\abs{1+e^{R+\imagi t}}} \\
    \quad \leq 2 \pi \max_{t \in \intcc{0}{2\pi}} \frac{e^{a R}}{\abs{1 - \abs{e^{R+\imagi t}}}}
    = 2 \pi \frac{e^{a R}}{e^R - 1}
    = 2 \pi \frac{e^{(a-1) R}}{1 - e^{-R}}
    \tendsasp{R} 0
  \end{IEEEeqnarray*}
  The final step holds since $a < 1$.
  Likewise we treat the left vertical part
  (replace $R$ by $-R$ and note $\abs{1-\abs{e^{-R+\imagi t}}} = 1 - e^{-R}$):
  \begin{IEEEeqnarray*}{0l}
    \abs{\int_{\gam_4} f(z) \de z}
    \leq 2 \pi \frac{e^{-a R}}{1 - e^{-R}}
    \tendsasp{R} 0
  \end{IEEEeqnarray*}
  The final step holds since due to $0 < a$, the numerator tends to~$0$,
  while the denominator tends to~$1$.
  \par
  The integrals along the horizontal parts are connected like this:
  \begin{IEEEeqnarray*}{0l}
    \int_{\gam_1} f(z) \de z
    = \int_{-R}^R \frac{e^{ax}}{1+e^x} \de x
    = \int_{-R}^R \frac{e^{ax}}{1+e^{x+2\pi\imagi}} \de x
    = e^{-a2\pi\imagi} \int_{-R}^R \frac{e^{a(x + 2 \pi\imagi)}}{1+e^{x+2\pi\imagi}} \de x \\
    = - e^{-a2\pi\imagi} \int_{R}^{-R} \frac{e^{a(x + 2 \pi\imagi)}}{1+e^{x+2\pi\imagi}} \de x
    = - e^{-a2\pi\imagi} \int_{\gam_3} f(z) \de z
  \end{IEEEeqnarray*}
  It follows:
  \begin{IEEEeqnarray*}{0l}
    \int_{\gam_1} f(z) \de z
    = \int_{\gam} f(z) \de z - \int_{\gam_2} f(z) \de z - \int_{\gam_3} f(z) \de z - \int_{\gam_4} f(z) \de z \\
    \quad = - 2 \pi \imagi e^{a \pi \imagi} - \int_{\gam_2} f(z) \de z
    + e^{a2\pi\imagi} \int_{\gam_1} f(z) \de z - \int_{\gam_4} f(z) \de z
  \end{IEEEeqnarray*}
  Hence (note $e^{a2\pi\imagi} \neq 1$ since $0 < a < 1$):
  \begin{IEEEeqnarray*}{0l}
    \int_{-R}^{R} \frac{e^{ax}}{1+e^x} \de x
    = \int_{\gam_1} f(z) \de z \\
    \quad = \frac{1}{1-e^{a2\pi\imagi}} \parens{-2\pi\imagi e^{a \pi \imagi}
      - \int_{\gam_2} f(z)\de z - \int_{\gam_4} f(z)\de z }
    \tendsasp{R} \frac{- 2 \pi \imagi e^{a \pi \imagi}}{1-e^{a2\pi\imagi}}
  \end{IEEEeqnarray*}
  The last term can be written shortly:
  \begin{IEEEeqnarray*}{0l}
    \frac{- 2 \pi \imagi e^{a \pi \imagi}}{1-e^{a2\pi\imagi}}
    = \frac{2 \pi \imagi e^{a \pi \imagi}}{e^{a2\pi\imagi} - 1}
    = \frac{2 \pi \imagi}{e^{a\pi\imagi} - e^{-a\pi\imagi}}
    = \frac{2 \pi \imagi}{2\imagi \sin(a\pi)}
    = \frac{\pi}{\sin(a\pi)}
  \end{IEEEeqnarray*}
\end{example}

\exercisesection{cha:cha070}

\begin{exercise}
  Compute $\int_{\ninfty}^\pinfty \frac{1}{(x^2+1)^4} \de x$.
\end{exercise}

\begin{exercise}
  Let $a > 1$.
  Compute $\int_0^{2\pi} \frac{1}{a+\sin(x)} \de x$.
\end{exercise}

\begin{exercise}
  Show that \CIFxx is a special case of the Residue Theorem.
\end{exercise}

\solutionsection

\begin{solution}
  We procede as in \autoref{ex:cha070:x2+1}
  The only difference is the computation of the integral along $\gam(R)$ with the Residue Theorem.
  Our function~is:
  \begin{IEEEeqnarray*}{0l}
    f \df \fn{z}{\CC \setminus \set{\pm\imagi}}%
    {\frac{1}{(z^2+1)^4} = \frac{1}{(z-\imagi)^4 (z+\imagi)^4}}
  \end{IEEEeqnarray*}
  The relevant pole is $\imagi$ of order~$4$.
  We have:
  \begin{IEEEeqnarray*}{0l}
    \res(f,\imagi)
    = \frac{1}{3!} \frac{\de^3}{\de z^3} \parens{z \mapsto \frac{1}{(z+\imagi)^4}}(\imagi)
    = \frac{1}{6} \parens{z \mapsto \frac{(-4)(-5)(-6)}{(z+\imagi)^7}}(\imagi)
    = \frac{15}{96 \imagi}
  \end{IEEEeqnarray*}
  Multiplication with $2\pi\imagi$ yields:
  \begin{IEEEeqnarray*}{0l}
    \int_{\ninfty}^\pinfty \frac{1}{(x^2+1)^4} \de x = \frac{15\pi}{48}
  \end{IEEEeqnarray*}
\end{solution}
\smallskip

\begin{solution}
  We have:
  \begin{IEEEeqnarray*}{0l}
    \int_0^{2\pi} \frac{1}{a+\sin(x)} \de x
    = \int_0^{2\pi} \frac{2\imagi}{2a\imagi+e^{\imagi x}-e^{-\imagi x}} \de x
    = \int_0^{2\pi} \frac{2 \ccdot \imagi e^{\imagi x}}{2a\imagi e^{\imagi x} + (e^{\imagi x})^2 - 1} \de x \\
    \quad = \int_{\kappa(0,1)} \frac{2}{2a\imagi z + z^2 - 1} \de z
    = \int_{\kappa(0,1)} \frac{2}{z^2 + 2a\imagi z - 1} \de z
  \end{IEEEeqnarray*}
  The denominator has the following zeros:
  \begin{IEEEeqnarray*}{0l}
    -a \imagi \pm \imagi \sqrt{a^2 - 1}
    = - \imagi \ccdot \parens{a \pm \sqrt{a^2 - 1}}
  \end{IEEEeqnarray*}
  The zero $- \imagi \ccdot \parens{a + \sqrt{a^2 - 1}}$ is located outside the circle since $a>1$.
  With simple real calculus,
  it can be seen that $\zeta \df - \imagi \ccdot \parens{a - \sqrt{a^2 - 1}}$ is inside of the circle,
  hence we have to consider this singularity.
  The denominator has order~$1$ in $\zeta$, hence $\zeta$ is a pole of order~$1$.
  We compute the residue:
  \begin{IEEEeqnarray*}{0l}
    \res\parens{z \mapsto \frac{2}{z^2 + 2a\imagi z - 1}, \: \zeta}
    = \frac{2}{\frac{\de}{\de z}\parens{z \mapsto z^2 + 2a\imagi z - 1}(\zeta)} \\
    \quad = \frac{2}{2 \zeta + 2 a \imagi} = \frac{1}{\imagi\sqrt{a^2-1}}
  \end{IEEEeqnarray*}
  Multiplication with $2\pi\imagi$ yields:
  \begin{IEEEeqnarray*}{0l}
    \int_0^{2\pi} \frac{1}{a+\sin(x)} \de x = \frac{2\pi}{\sqrt{a^2-1}}
  \end{IEEEeqnarray*}
\end{solution}
\smallskip

\begin{solution}
  Let $\Om \subseteq \CC$ be a domain and $\Gam$ a cycle in $\Om$ that is NH in~$\Om$.
  Let $f \in \fnset{\Om}{\CC}$ be holomorphic.
  Let $n \in \NNzero$ and $z \in \Om \setminus \img(\Gam)$.
  We have to show:
  \begin{IEEEeqnarray*}{0l}
    \ind_{\Gam}(z) \ccdot f^{(n)}(z) = \frac{n!}{2\pi\imagi} \int_\Gam \frac{f(\zeta)}{(\zeta-z)^{n+1}} \de \zeta
  \end{IEEEeqnarray*}
  Define:
  \begin{IEEEeqnarray*}{0l}
    g \df \fn{\zeta}{\Om \setminus \set{z}}{\frac{f(\zeta)}{(\zeta-z)^{n+1}}}
  \end{IEEEeqnarray*}
  Then $g$ is holomorphic and has a pole of order $p \leq n+1$ in $z$.
  The holomorphic extension of $\fn{\zeta}{\Om \setminus \set{z}}{(\zeta - z)^{n+1} \ccdot g(\zeta)}$
  is the function $f$. Hence:
  \begin{IEEEeqnarray*}{0l}
    \res(g,z) = \taylorp{f}{n+1-1}{z} = \taylor{f}{n}{z}
  \end{IEEEeqnarray*}
  The cycle $\Gam$ is NH in $\Om$,
  which is the domain of $g$ including its singularity~$z$.
  The Residue Theorem yields:
  \begin{IEEEeqnarray*}{0l}
    \int_\Gam \frac{f(\zeta)}{(\zeta-z)^{n+1}} \de \zeta = \int_\Gam g(\zeta) \de \zeta
    = 2\pi\imagi \ccdot \res(g,z) \ccdot \ind_\Gam(z)
    = 2\pi\imagi \taylor{f}{n}{z} \ccdot \ind_\Gam(z)
  \end{IEEEeqnarray*}
\end{solution}

%%% Local Variables:
%%% TeX-master: "Complex_Analysis_NatAppSci.tex"
%%% End:
