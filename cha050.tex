\chapter{Cauchy Integral Formula II}
\label{cha:cha050}

\section{Winding Number}

\begin{para}
  Let $\gam$ be a curve and define $\imgC(\gam) \df \CC \setminus \img(\gam)$
  the complement of its trace.
  Since $\img(\gam)$ is closed, $\imgC(\gam)$ is open.
  The components of $\imgC(\gam)$ are also called the \term{components} of $\gam$.
  Define:
  \begin{IEEEeqnarray*}{0l}
    \ind_{\gam} \df \fn{z}{\imgC(\gam)}{\frac{1}{2\pi\imagi} \int_\gam \frac{1}{\zeta - z} \de \zeta}
  \end{IEEEeqnarray*}
  For all $z \in \imgC(\gam)$, we call $\ind_{\gam}(z)$ the \term{winding number} of $\gam$ around $z$.
\end{para}

\begin{para}
  \label{para:cha050:n-gam}
  For later, we record the following notation.
  Let $\gam$ be a closed curve and $n \in \NN$.
  Denote:
  \begin{IEEEeqnarray*}{0l}
    n \gam \df \gam \cat \hdots \cat \gam
  \end{IEEEeqnarray*}
  This means we form a new closed curve by running through $\gam$ for a number of $n$ times.
  For all $n \in \ZZneg$, denote $n \gam \df (-n) (-\gam)$,
  that is, the inverse closed curve $-\gam$ is run a number of $-n$ times.
  Finally, $0 \gam$ is defined by $\img(0 \gam) = \set{\al(\gam)}$, called a \term{dummy curve}.
  With those definitions,
  we have $\ind_{n\gam}(z) = n \ccdot \ind_{\gam}(z)$ for all $n \in \ZZ$ and all $z \in \imgC(\gam)$.
  If $\Om \subseteq \CC$ is open with $\img(\gam) \subseteq \Om$
  and $f \in \fnset{\Om}{\CC}$ is continuous,
  then $\int_{n \gam} f(\zeta) \de \zeta = n \int_{\gam} f(\zeta) \de \zeta$.
\end{para}

\begin{para}
  A \term{cycle} is a finite bag of closed curves.\footnote{
    This is not to be confused with the \emphasis{circle} $\kappa(z_{0},r)$ around $z_{0}$ with radius $r$.
    Perhaps, a better terminology would be to call a closed curve a \term{cycle}
    (and then not to use this word too often) and then call a finite bag of cycles a \term{multicycle}.}
  Let $\Gam$ be a cycle. Then we naturally define:
  \begin{IEEEeqnarray*}{0l+l}
    \img(\Gam) \df \bigcup_{\gam \in \Gam} \img(\gam) &
    \imgC(\Gam) \df \CC \setminus \img(\Gam)
  \end{IEEEeqnarray*}
  The components of $\Gam$ are the components of $\imgC(\Gam)$.
  We also carry over the winding number:
  \begin{IEEEeqnarray*}{0l}
    \ind_{\Gam} = \fn{z}{\imgC(\Gam)}{\sum_{\gam \in \Gam} \ind_{\gam}(z)}
  \end{IEEEeqnarray*}
  Let $\Om \subseteq \CC$. We say that $\Gam$ is a \term{cycle in} $\Om$ if $\img(\Gam) \subseteq \Om$.
  If $f \in \fnset{\Om}{\CC}$ is continuous and $\Gam$ is a cycle in $\Om$, we define:
  \begin{IEEEeqnarray*}{0l}
    \int_{\Gam} f(z) \de z \df \sum_{\gam \in \Gam} \int_{\gam} f(z) \de z
  \end{IEEEeqnarray*}
\end{para}

\begin{proposition}
  \label{prop:cha050:circle-1}
  Let $r > 0$ and $z_{0} \in \CC$. Then $\ind_{\kappa(z_{0},r)}(z) = 1$ for all $z \in D(z_{0},r)$.
\end{proposition}

\begin{proof}
  Apply \CIFx to the constant function $f=1$.
\end{proof}

\begin{proposition}
  Let $\gam$ be a curve. Then $\ind_{\gam}$ is continuous.
  Clearly, the same holds for $\ind_{\Gam}$ if $\Gam$ is a cycle,
  since then $\ind_{\Gam}$ is a sum of continuous functions.
\end{proposition}

\begin{proof}
  Let $\veps > 0$ and $z \in \imgC(\gam)$.
  Let $r > 0$ be such that $\clD(z,r) \subseteq \imgC(\gam)$;
  since $\imgC(\gam)$ is open, we find $r' > 0$ such that $D(z,r) \subseteq \imgC(\gam)$,
  then choose any $r \in \intoo{0}{r'}$. Define:
  \begin{IEEEeqnarray*}{0l}
    M \df \max\set{\tfrac{1}{\abs{\zeta-w}} \suchthat \zeta \in \img(\gam) \land w \in \clD(z,r)}
  \end{IEEEeqnarray*}
  This maximum exists since $\img(\gam)$ and $\clD(z,r)$ are compact.
  Define:
  \begin{IEEEeqnarray*}{0l}
    \del \df \min\set{r, \frac{\veps}{M^2 L(\gam)}}
  \end{IEEEeqnarray*}
  Then for all $w \in D(z,\del)$, we have:
  \begin{IEEEeqnarray*}{0l+x*}
    \abs{\ind_\gam(w) - \ind_\gam(z)}
    = \frac{1}{2 \pi} \abs{\int_\gam \frac{1}{\zeta - w} - \frac{1}{\zeta - z} \de \zeta} \\
    \quad \leq L(\gam) \max_{\zeta \in \img(\gam)} \abs{\frac{1}{\zeta - w} - \frac{1}{\zeta - z}} \\
    \quad = L(\gam) \max_{\zeta \in \img(\gam)} \frac{\abs{z-w}}{\abs{\zeta - w}\ccdot\abs{\zeta - z}}
    < L(\gam) \del M^2 = \veps & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}
  Let $K \subseteq \CC$ be a bounded set, for example the trace of a cycle.
  Then $\CC \setminus K$ has exactly one component that is unbounded.
  To see this, let $R > 0$ such that $K \subseteq D(0,R)$.
  Then $\CC \setminus D(0,R)$ belongs to one component of $\CC \setminus K$,
  which is clearly unbounded.
  All other components are inside $D(0,R)$ and hence bounded.
\end{para}

\begin{proposition}
  Let $\Gam$ be a cycle.
  We have:
  \begin{enumerate}
  \item $\ind_\Gam(z) \in \ZZ$ for all $z \in \imgC(\Gam)$
  \item $\ind_\Gam$ is constant on each component of $\imgC(\Gam)$.
    For all $z$ from the unbounded component, we have $\ind_\Gam(z)=0$.
  \end{enumerate}
\end{proposition}

\begin{proof}
  \begin{enumerate}
  \item It suffices to prove the statement for each closed curve in the cycle.
    Let $\gam = (\eli{\gam}{n}) \in \Gam$ and $k \in \setn{n}$.
    We may assume $\gam_k \in \fnset{\intcc{0}{1}}{\CC}$,
    otherwise we do a parameter transformation, which does not change any curve integrals.
    Let $z \in \imgC(\Gam)$.
    Define two auxiliary functions:
    \begin{IEEEeqnarray*}{0l+l}
      h_k \df \fn{t}{\intcc{0}{1}}%
      {\frac{1}{2\pi\imagi} \int_{\fnres{\gam_k}{\intcc{0}{t}}} \frac{1}{\zeta - z} \de \zeta} &
      \tih_k \df \fn{t}{\intcc{0}{1}}{\frac{e^{2\pi\imagi \ccdot h_k(t)}}{\gam_k(t) - z}}
    \end{IEEEeqnarray*}
    For all $t \in \intcc{0}{1}$ we have:
    \begin{IEEEeqnarray*}{0l}
      h_k(t) = \frac{1}{2\pi\imagi} \int_0^t \frac{\gam_k'(s)}{\gam_k(s) - z} \de s
    \end{IEEEeqnarray*}
    By the Fundamental Theorem, it follows
    $h_k'(t) = \frac{1}{2\pi\imagi} \frac{\gam_k'(t)}{\gam_k(t) - z}$, and hence:
    \begin{IEEEeqnarray*}{0l}
      \tih_k'(t) = \frac{2\pi\imagi \ccdot h_k'(t) \ccdot e^{2\pi\imagi \ccdot h_k(t)}}{\gam_k(t) - z}
      - \frac{\gam_k'(t) \ccdot e^{2\pi\imagi \ccdot h_k(t)}}{(\gam_k(t) - z)^2}
      = 0
    \end{IEEEeqnarray*}
    Hence $\tih_k$ is constant, so in particular:
    \begin{IEEEeqnarray*}{0l}
      \frac{e^{2\pi\imagi \ccdot h_k(1)}}{\gam_k(1) - z}
      = \frac{e^{2\pi\imagi \ccdot h_k(0)}}{\gam_k(0) - z} = \frac{1}{\gam_k(0) - z}
    \end{IEEEeqnarray*}
    We apply this to the whole closed curve:
    \begin{IEEEeqnarray*}{0l}
      e^{2 \pi\imagi \ccdot \ind_\gam(z)}
      = e^{2 \pi\imagi \sum_{k=1}^n \ind_{\gam_{k}}(z)}
      = e^{2 \pi\imagi \sum_{k=1}^n h_k(1)}
      = \prod_{k=1}^n e^{2 \pi\imagi \ccdot h_k(1)} \\
      \quad = \prod_{k=1}^n \frac{\gam_k(1) - z}{\gam_k(0) - z}
      = \frac{\gam_n(1) - z}{\gam_n(0) - z} \ccdot \prod_{k=1}^{n-1} \frac{\gam_{k+1}(0) - z}{\gam_k(0) - z}
      = \frac{\gam_n(1) - z}{\gam_n(0) - z} \ccdot \frac{\gam_{n}(0) - z}{\gam_1(0) - z} = 1
    \end{IEEEeqnarray*}
    The final equality holds since $\gam$ is closed, that is, $\gam_1(0) = \gam_n(1)$.
    The claim now follows from Euler's formula.
  \item Constancy on each component follows easily from continuity and the fact
    that only integer values are assumed (for details, see \autoref{exc:cha050:const}).
    Now let $R > 0$ be such that $\img(\Gam) \subseteq D(0,R)$;
    such $R$ exists since $\img(\Gam)$ is compact.
    Then for all $z$ with $\abs{z} > R$ and all $\gam \in \Gam$, we have:
    \begin{IEEEeqnarray*}{0l}
      \abs{\ind_\gam(z)}
      \leq L(\gam) \max_{\zeta \in \img(\gam)} \frac{1}{\abs{\zeta - z}}
      \leq L(\gam) \max_{\zeta \in \img(\gam)} \frac{1}{\abs{\abs{\zeta} - \abs{z}}} \\
      \quad = L(\gam) \max_{\zeta \in \img(\gam)} \frac{1}{\abs{z} - \abs{\zeta}}
      \leq L(\gam) \frac{1}{\abs{z} - R}
    \end{IEEEeqnarray*}
    For $\abs{z} \tends \infty$, the right\-/hand side tends to~$0$,
    so we find $z$ in the unbounded component so that
    $\abs{\ind_\gam(z)} < 1$ hence $\ind_\gam(z) = 0$ since only integer values are assumed.
    Since $\ind_{\gam}$ is constant on the unbounded component of $\img(\gam)$,
    it follows that it is~$0$ on this component.
    The claim follows since the unbounded component of $\img(\Gam)$
    is contained in the unbounded component of $\img(\gam)$ for all $\gam \in \Gam$.\qedhere
  \end{enumerate}
\end{proof}

\begin{figure}[t]
  \centering
  \includegraphics[width=\textwidth]{plot/cha050-crossing.pdf}
  \caption{Situation of the crossing lemma.}
  \label{fig:cha050:crossing}
\end{figure}

\begin{proposition}[Crossing Lemma]
  \label{prop:cha050:crossing}
  Let $\gam = (\eli{\gam}{n})$ be a closed curve
  and ${z_0 \in \CC}$ and $r > 0$, denote $D \df D(z_{0},r)$.
  Assume that there are $j \in \setn{n}$ and $a,b \in \RR$
  such that denoting $\bet \df \fnres{\gam_{j}}{\intcc{a}{b}}$, we have:
  \begin{itemize}
  \item $\bet$ is the only part of $\gam$ whose trace intersects with $\overline{D}$;
  \item $\partial D \cap \img(\bet)$ consists of exactly two points,
    namely $\al(\bet)=\beta(a)$ and ${\om(\bet)=\bet(b)}$;
  \item $\bet$ divides $D$ in exactly two components,
    that is, ${D \setminus \img(\bet)}$ has exactly two components.
  \end{itemize}
  Let $z_1$ and $z_2$ be in different components of $D \setminus \img(\bet)$ such that
  from the point of view of $z_1$, the smooth curve $\bet$ runs from left to right through $D$,
  where the direction of $\bet$ can be determined
  by looking at the location of $\al(\bet)$ and $\om(\bet)$ relatively to $z_{1}$.
  \par
  Then $\ind_\gam(z_2) = \ind_\gam(z_1) + 1$ holds.
\end{proposition}

\begin{para}
  Before we prove the crossing lemma, we look at its statement more closely.
  The situation is depicted in \autoref{fig:cha050:crossing}.
  We can think of $\gam$ as a river.
  Whenever we cross the river, the winding number increases or decreases by~$1$,
  depending on the direction of flow.
  When we cross the river in a direction such that the flow of water comes from the left,
  then the winding number increases by~$1$; otherwise it decreases by~$1$.
  It must be noted that we may only cross the river in places where the situation is clearly arranged,
  that is, the river comes from one side and moves on to the other side.
  The crossing lemma allows to compute the winding number
  in each component of appropriately behaved closed curves:
  we start in the unbounded component and then work our way inwards to the other components.
\end{para}

\begin{proof}[of the crossing lemma]
  Denote $\tibet$ the part of $\gam$ that runs from $\om(\bet)$ to~$\al(\bet)$.
  Let $\kappa_1$ and $\kappa_2$ be counter\-/clockwise parameterizations
  of the marked parts (see \autoref{fig:cha050:crossing}) of the circle around $z_0$ with radius $r$.
  We have:
  \begin{IEEEeqnarray*}{0l"s+x*}
    \ind_\gam(z_1) + 1 \\
    \quad = \ind_{\bet \cat \tibet}(z_1) + \ind_{\kappa_1 \cat \kappa_2}(z_1) & \autoref{prop:cha050:circle-1} \\
    \quad = \ind_{\kappa_1 \cat \tibet}(z_1) + \ind_{\bet \cat \kappa_2}(z_1) & additivity of integral \\
    \quad = \ind_{\kappa_1 \cat \tibet}(z_1) & $z_1$ in unbounded component \\
    \quad = \ind_{\kappa_1 \cat \tibet}(z_2) & $z_1$ and $z_2$ in same component \\
    \quad = \ind_{\kappa_1 \cat \tibet}(z_2) + \ind_{\bet \cat -\kappa_1}(z_2) & $z_2$ in unbounded component \\
    \quad = \ind_{\bet \cat \tibet}(z_2) + \ind_{\kappa_1 \cat -\kappa_1}(z_2) & additivity of integral \\
    \quad = \ind_{\bet \cat \tibet}(z_2) & $z_2$ in unbounded component \\
    \quad = \ind_{\gam}(z_2) && \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\section{The Formula}

\begin{para}
  Let $\Om \subseteq \CC$ and $\Gam$ be a cycle in~$\Om$.
  We call $\Gam$ \term{null\-/homologous} (NH) in $\Om$,
  if $\ind_\Gam(z) = 0$ holds for all $z \in \CC \setminus \Om$.
\end{para}

We need a technical preparation:

\begin{proposition}
  \label{prop:cha050:param-int}
  Let $\Om \subseteq \CC$ be open and $\gam$ a curve in $\CC$. Let
  \begin{IEEEeqnarray*}{0l}
    f \in \fnset{\Om \times \img(\gam)}{\CC}
  \end{IEEEeqnarray*}
  be continuous.
  For all fixed $\zeta \in \img(\gam)$ let $f_{\zeta} \df \fn{z}{\Om}{f(z,\zeta)}$ be holomorphic.
  Finally let the function $\fn{(z,\zeta)}{\Om \times \img(\gam)}{f'_{\zeta}(z)}$ be continuous.
  Then the function
  \begin{IEEEeqnarray*}{0l}
    F \df \fn{z}{\Om}{\int_\gam f(z,\zeta) \de \zeta}
  \end{IEEEeqnarray*}
  is holomorphic, and for all $z \in \Om$, we have:
  \begin{IEEEeqnarray*}{0l}
    F'(z) = \int_\gam f'_{\zeta}(z) \de \zeta
  \end{IEEEeqnarray*}
\end{proposition}

\begin{proof}
  A similar result is typically treated in an advanced course for real integration.
  The complex version can be deduced from that.
  We omit the details.
\end{proof}

\begin{theorem}[Cauchy Integral Formula II (\CIFxx)]
  Let $\Om \subseteq \CC$ be a domain and $\Gam$ a cycle in $\Om$ that is NH in~$\Om$.
  Let $f \in \fnset{\Om}{\CC}$ be holomorphic. Then we have:
  \begin{IEEEeqnarray*}{0l}
    \int_\Gam f(\zeta) \de \zeta = 0
  \end{IEEEeqnarray*}
  Moreover, for all $n \in \NNzero$ and all $z \in \Om \setminus \img(\Gam)$:
  \begin{IEEEeqnarray*}{0l}
    \ind_{\Gam}(z) \ccdot f^{(n)}(z) = \frac{n!}{2\pi\imagi} \int_\Gam \frac{f(\zeta)}{(\zeta-z)^{n+1}} \de \zeta
  \end{IEEEeqnarray*}
  In particular:
  \begin{IEEEeqnarray*}{0l+l}
    \ind_{\Gam}(z) \ccdot f(z) = \frac{1}{2\pi\imagi} \int_\Gam \frac{f(\zeta)}{\zeta-z} \de \zeta &
    \ind_{\Gam}(z) \ccdot f'(z) = \frac{1}{2\pi\imagi} \int_\Gam \frac{f(\zeta)}{(\zeta-z)^{2}} \de \zeta
  \end{IEEEeqnarray*}
\end{theorem}

\begin{proof}
  We only give a proof sketch.
  For the case $n=0$, we have to show for all $z \in \Om \setminus \img(\Gam)$:
  \begin{IEEEeqnarray*}{0c}
    0 = \frac{1}{2\pi\imagi} \int_\Gam \frac{f(\zeta)}{\zeta-z} \de \zeta - \ind_{\Gam}(z) \ccdot f(z)
    = \frac{1}{2\pi\imagi} \int_\Gam \frac{f(\zeta) - f(z)}{\zeta-z} \de \zeta
    \Df h(z)
  \end{IEEEeqnarray*}
  The function $h$ is defined on $\Om \setminus \img(\Gam)$,
  but using that $\Gam$ is NH and using \autoref{prop:cha040:morera-cor},
  $h$ can be extended to an entire function~$H$.
  Then we show that $H$ is bounded, by Liouville's theorem hence constant.
  Moreover, we show that $\abs{H(z)} \tends 0$ whenever $\abs{z} \tends \pinfty$,
  so $H=0$ is the only possibility.
  This concludes the main part of the proof.
  \par
  The case $n \geq 1$ can be easily deduced from this by \autoref{prop:cha050:param-int},
  see \autoref{exc:cha050:CIF-proof-1}.
  How to finally obtain $\int_\Gam f(\zeta) \de \zeta = 0$
  is also easy and will be considered in \autoref{exc:cha050:CIF-proof-2}.
\end{proof}

\begin{para}
  The condition that $\Gam$ is NH in the domain of the function is essential,
  as the following example shows.
  Define $f \df \fn{z}{\CCnz}{\frac{1}{z}}$ and $\gam \df \kappa(0,1)$.
  Then $\gam$ is not NH in $\CCnz$, since $\ind_{\gam}(0) = 1$.
  By \autoref{exc:cha040:int-a-b} with $a=0$ and $b=2$:
  \begin{IEEEeqnarray*}{0l}
    \frac{1}{2\pi\imagi} \int_\gam \frac{f(\zeta)}{\zeta-2} \de \zeta
    = \frac{1}{2\pi\imagi} \int_\gam \frac{1}{(\zeta-0) (\zeta-2)} \de \zeta
    = \frac{1}{0-2} = -\frac{1}{2}
  \end{IEEEeqnarray*}
  On the other hand, \CIFxx\ --~neglecting the NH prerequisite~--
  says that this should be $\ind_{\gam}(2) \ccdot f(2) = 0 \ccdot f(2) = 0$,
  a contradiction.
  \par
  In addition, we look at a point inside of the circle.
  Again, neglecting the NH prerequisite, by \CIFxx:
  \begin{IEEEeqnarray*}{0l}
    2 = 1 \cdot f\parens{\frac{1}{2}} = \ind_{\gam}\parens{\frac{1}{2}} \cdot f\parens{\frac{1}{2}}
    = \frac{1}{2\pi\imagi} \int_\gam \frac{1}{\zeta (\zeta-\frac{1}{2})} \de \zeta
  \end{IEEEeqnarray*}
  It follows $\int_\gam \frac{1}{\zeta (\zeta-\frac{1}{2})} \de \zeta = 4 \pi \imagi$.
  But this is wrong. To see this, we have to compute the value of this integral.
  In the next chapters, we will learn how to do this with the Residue Theorem.
  We will then see that the actual value of this integral is:
  \begin{IEEEeqnarray*}{0l}
    \int_\gam \frac{1}{\zeta (\zeta-\frac{1}{2})} \de \zeta
    = 2 \pi \imagi (-2 + 2) = 0
  \end{IEEEeqnarray*}
\end{para}

\exercisesection{cha:cha050}

\begin{exercise}
  \label{exc:cha050:const}
  Give the details for the proof that the winding number is constant on each component.
\end{exercise}

\begin{exercise}
  Let $r \in \fnset{\intcc{0}{1}}{\RRpos}$ and $\phi \in \fnset{\intcc{0}{1}}{\RR}$
  be continuously differentiable with $r(1) = r(0)$ and $\phi(1) = \phi(0) + 2\pi m$ for some $m \in \ZZ$.
  Define:
  \begin{IEEEeqnarray*}{0l}
    \gam \df \fn{t}{\intcc{0}{1}}{r(t) e^{\imagi \phi(t)}}
  \end{IEEEeqnarray*}
  Prove --~by a computation~-- that $\ind_\gam(0) = m$,
  which is in accordance with the intuitive understanding of the winding number.
\end{exercise}

\begin{exercise}
  Define $z_1 \df 3 + \imagi$ and $z_2 \df \frac{\imagi}{2}$ and $z_3 \df 4\imagi - 1$.
  Let $\gam$ be the closed curve given in the following figure:
  \begin{center}
    \includegraphics[width=0.4\textwidth]{plot/cha050-exercise-curve.pdf}
  \end{center}
  Compute for all $j \in \set{1,2,3}$ and all $k \in \set{1,2,3,4}$ the following integral with~\CIFxx:
  \begin{IEEEeqnarray*}{0l}
    \int_\gam \frac{\zeta^2}{(\zeta - z_j)^{k}} \de \zeta
  \end{IEEEeqnarray*}
\end{exercise}

\begin{exercise}
  \label{exc:cha050:circles-equal}
  Let $\Om \subseteq \CC$ be a domain, and let $f \in \fnset{\Om}{\CC}$ be holomorphic.
  Let $\gam_1$ and $\gam_2$ be closed curves in $\Om$ such that
  $\Gam \df \set{\gam_1, -\gam_2}\isbag$ is NH in $\Om$.
  Prove that $\int_{\gam_1} f(z) \de z = \int_{\gam_2} f(z) \de z$.
  Then give a simple example for such a situation.
\end{exercise}

\begin{exercise}
  \label{exc:cha050:CIF-proof-1}
  Show how the case $n \geq 1$ in the proof of \CIFxx
  can be handled using \autoref{prop:cha050:param-int}.
\end{exercise}

\begin{exercise}
  \label{exc:cha050:CIF-proof-2}
  Show how in the proof of \CIFxx,
  we can obtain the statement $\int_\Gam f(\zeta) \de \zeta = 0$ from the actual integral formula.
\end{exercise}

\solutionsection

\begin{solution}
  Let $\Om \subseteq \CC$ be connected, and let $f \in \fnset{\Om}{\ZZ}$ be continuous.
  We show that $f$ is constant.
  For contradiction, assume that there are ${z, w \in \Om}$ with $f(z) \neq f(w)$.
  Let $\gam \in \fnset{\intcc{a}{b}}{\Om}$ be a curve in $\Om$ from $z$ to $w$.
  Denote $t_{0} \df \sup \set{t \in \intcc{a}{b} \suchthat f(\gam(t)) = f(z)}$.
  Then $t_{0} \in \intcc{a}{b}$, so we can look at ${z_{0} \df \gam(t_{0})}$.
  \begin{itemize}
  \item Case $f(z_{0}) = f(z)$, that is, the supremum is in fact a maximum.
    Then $t_{0} < b$.
    For all $\veps > 0$, we have $D(z_{0}, \veps) \cap \img(\fnres{\gam}{\intoc{t_{0}}{b}}) \neq \emptyset$
    due to the continuity of $\gam$ (recall that each curve can be considered
    as a continuous function from a compact interval to $\CC$).
    At the same time, $f(\zeta) \neq f(z)$ for all $\zeta \in \img(\fnres{\gam}{\intoc{t_{0}}{b}})$.
    So in each neighborhood of $z_{0}$, there are points where $f$ assumes $f(z)$
    and also some other value.
    Since all those values are integers, we have a contradiction to continuity of $f$.
  \item Case $f(z_{0}) \neq f(z)$.
    Then $a < t_{0}$.
    We repeat the above argument using $\fnres{\gam}{\intco{a}{t_{0}}}$.
  \end{itemize}
\end{solution}

\begin{solution}
  We have:
  \begin{IEEEeqnarray*}{0l}
    2 \pi \imagi \ccdot \ind_\gam(0)
    = \int_\gam \frac{1}{\zeta - 0} \de \zeta
    = \int_0^1 \frac{\gam'(t)}{\gam(t)} \de t
    = \int_0^1 \frac{r'(t) e^{\imagi \phi(t)} + r(t) \imagi \phi'(t) e^{\imagi \phi(t)}}{r(t) e^{\imagi \phi(t)}} \de t \\
    \quad = \int_0^1 \frac{r'(t) + r(t) \imagi \phi'(t)}{r(t)} \de t
    = \int_0^1 \frac{r'(t)}{r(t)} \de t + \imagi \int_0^1 \phi'(t) \de t \\
    \quad = \fneval{\ln(r(t))}{t=0}{1} + \imagi \fneval{\phi(t)}{t=0}{1}
    = 0 + \imagi \fneval{\phi(t)}{t=0}{1}
    = \imagi 2\pi m
  \end{IEEEeqnarray*}
\end{solution}

\begin{solution}
  Define $\Om \df \CC$ and $f \df \fn{\zeta}{\Om}{\zeta^{2}}$.
  Then, clearly, $\gam$ is NH in~$\Om$, since $\CC \setminus \Om = \emptyset$.
  Using the crossing lemma, we determine $\ind_\gam(z_1) = 1$ and $\ind_\gam(z_2) = 2$ and $\ind_\gam(z_3) = 0$,
  and none of these three points is located on the trace of $\gam$.
  It follows with \CIFxx for $k=1$:
  \begin{IEEEeqnarray*}{0l}
    \int_\gam \frac{\zeta^2}{\zeta - z_1} \de \zeta
    = 2 \pi \imagi \ccdot 1 \ccdot f(z_{1})
    = 2 \pi \imagi (8+6\imagi) = 4 \pi (4\imagi - 3) \\
    \int_\gam \frac{\zeta^2}{\zeta - z_2} \de \zeta
    = 2 \pi \imagi \ccdot 2 \ccdot f(z_{2})
    = 4 \pi \imagi \frac{-1}{4} = - \pi \imagi \\
    \int_\gam \frac{\zeta^2}{\zeta - z_3} \de \zeta
    = 2 \pi \imagi \ccdot 0 \ccdot f(z_{3}) = 0
  \end{IEEEeqnarray*}
  For $k=2$:
  \begin{IEEEeqnarray*}{0l}
    \int_\gam \frac{\zeta^2}{(\zeta - z_1)^{2}} \de \zeta
    = 2 \pi \imagi \ccdot 1 \ccdot f'(z_{1})
    = 2 \pi \imagi (6+2\imagi) = 4 \pi (3\imagi - 1) \\
    \int_\gam \frac{\zeta^2}{(\zeta - z_2)^{2}} \de \zeta
    = 2 \pi \imagi \ccdot 2 \ccdot f'(z_{2})
    = 4 \pi \imagi \imagi = - 4 \pi \\
    \int_\gam \frac{\zeta^2}{(\zeta - z_3)^{2}} \de \zeta
    = 2 \pi \imagi \ccdot 0 \ccdot f'(z_{3}) = 0
  \end{IEEEeqnarray*}
  For $k=3$:
  \begin{IEEEeqnarray*}{0l}
    \int_\gam \frac{\zeta^2}{(\zeta - z_1)^{3}} \de \zeta
    = 2 \pi \imagi \ccdot 1 \ccdot f''(z_{1})
    = 4 \pi \imagi \\
    \int_\gam \frac{\zeta^2}{(\zeta - z_2)^{3}} \de \zeta
    = 2 \pi \imagi \ccdot 2 \ccdot f''(z_{2})
    = 8 \pi \imagi \\
    \int_\gam \frac{\zeta^2}{(\zeta - z_3)^{3}} \de \zeta
    = 2 \pi \imagi \ccdot 0 \ccdot f''(z_{3}) = 0
  \end{IEEEeqnarray*}
  For $k=4$, we have the third derivative of $f$ to consider, which is~$0$,
  so all those integrals are~$0$.
\end{solution}

\begin{solution}
  By \CIFxx, we have $\int_\Gam f(z) \de z = 0$,
  hence $0 = \int_{\gam_1} f(z) \de z + \int_{-\gam_2} f(z) \de z
  = \int_{\gam_1} f(z) \de z - \int_{\gam_2} f(z) \de z$.
  A simple example is $f$ being defined on $\Om = \puD(z_{0},R)$.
  Then $\int_{\kappa(z_{0},r)} f(z) \de z = \int_{-\kappa(z_{0},s)} f(z) \de z$
  for all $r,s \in \intoo{0}{R}$.
\end{solution}

\begin{solution}
  We do induction on $n$.
  The induction base $n=0$ has already been completed.
  Now assume that the formula holds for some $n$, so we have to prove it for $n+1$.
  Define:
  \begin{IEEEeqnarray*}{0l}
    g \df \fn{z}{(\Om \setminus \img(\Gam)) \times \img(\Gam)}%
    {\frac{f(\zeta)}{(\zeta-z)^{n+1}}}
  \end{IEEEeqnarray*}
  Then $g$ is continuous.
  For all $\zeta \in \img(\Gam)$, we define:
  \begin{IEEEeqnarray*}{0l}
    g_{\zeta} \df \fn{z}{\Om \setminus \img(\Gam)}{g(z,\zeta)}
  \end{IEEEeqnarray*}
  Then $g_{\zeta}(z) = \frac{f(\zeta)}{(\zeta-z)^{n+1}}$ for all $z \in \Om \setminus \img(\Gam)$,
  so $g_{\zeta}$ is holomorphic with $g_{\zeta}'(z) = (n+1) \frac{f(\zeta)}{(\zeta-z)^{n+2}}$.
  The function $\fn{(z,\zeta)}{(\Om \setminus \img(\Gam)) \times \img(\Gam)}{g_{\zeta}'(z)}$
  is hence continuous.
  Define $G \df \fn{z}{\Om \setminus \img(\Gam)}{\int_{\Gam} g(z,\zeta) \de \zeta}$.
  By \autoref{prop:cha050:param-int}, it follows for all $z \in \Om \setminus \img(\Gam)$:
  \begin{IEEEeqnarray*}{0l}
    G'(z)
    = (n+1) \int_{\Gam} \frac{f(\zeta)}{(\zeta-z)^{n+2}} \de \zeta
    = (n+1) \int_{\Gam} \frac{f(\zeta)}{(\zeta-z)^{(n+1)+1}} \de \zeta
  \end{IEEEeqnarray*}
  At the same time, due to the induction hypothesis, we have:
  \begin{IEEEeqnarray*}{0l}
    G(z) = \int_{\Gam} \frac{f(\zeta)}{(\zeta-z)^{n+1}} \de \zeta
    = \frac{2\pi\imagi}{n!} \ccdot \ind_{\Gam}(z) \ccdot \diff{f}{n}(z)
  \end{IEEEeqnarray*}
  It follows:
  \begin{IEEEeqnarray*}{0l}
    \ind_{\Gam}(z) \ccdot \diff{f}{n+1}(z) = \frac{n!}{2\pi\imagi} G'(z)
    = \frac{n!}{2\pi\imagi} (n+1) \int_{\Gam} \frac{f(\zeta)}{(\zeta-z)^{(n+1)+1}} \de \zeta \\
    \quad = \frac{(n+1)!}{2\pi\imagi} \int_{\Gam} \frac{f(\zeta)}{(\zeta-z)^{(n+1)+1}} \de \zeta
  \end{IEEEeqnarray*}
\end{solution}

\begin{solution}
  Let $z_0 \in \Om \setminus \img(\Gam)$.
  Define $g \df \fn{z}{\Om}{(z-z_0) f(z)}$.
  Then $g$ is holomorphic, and $g(z_0) = 0$, and moreover:
  \begin{IEEEeqnarray*}{0l}
    \int_\Gam f(\zeta) \de \zeta = \int_\Gam \frac{g(\zeta)}{\zeta - z_0} \de \zeta
    = 2 \pi \imagi \ccdot \ind_{\Gam}(z_0) \ccdot g(z_0) = 0
  \end{IEEEeqnarray*}
\end{solution}

%%% Local Variables:
%%% TeX-master: "Complex_Analysis_NatAppSci.tex"
%%% End:
