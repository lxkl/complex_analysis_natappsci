\chapter{Systematic Application of the Residue Theorem}
\label{cha:cha080}

We present two general methods how the Residue Theorem can be applied
in order to compute improper real integrals.

\begin{para}
  Denote $\HH \df \set{z \in \CC \suchthat \Im(z) > 0}$ the upper open complex half\-/plane,
  then $\clHH = \set{z \in \CC \suchthat \Im(z) \geq 0}$ is its closure.
  Like in the last chapter, denote the curves ${\al(R) \df \tau(-R, R)}$
  and $\beta(R) \df \fnres{\kappa(0,R)}{\intcc{0}{\pi}}$ for all $R > 0$.
  Then the concatenation $\gam(R) \df \al(R) \cat \bet(R)$ is a closed curve,
  and $\ind_{\gam(R)}(z) = 1$ for all ${z \in D(0,R) \cap \HH}$
  and $\ind_{\gam(R)}(z) = 0$ for all other $z$ outside of the trace of~$\gam$.
\end{para}

\begin{para}
  \label{para:cha080:f}
  Let $\Om \subseteq \CC$ be a domain with $\clHH \subseteq \Om$,
  and let $S \subseteq \CC \setminus \RR$ be finite, denote $\Om_{S} \df \Om \setminus S$.
  Let $f \in \fnset{\Om_{S}}{\CC}$ be holomorphic.
  Note that no singularities of $f$ are located on the real axis,
  so $f$ is defined in all points of $\RR$.
  We call $R > 0$ \term{admissible}
  if $S \cap \HH$ is in the inner component of the semicircle defined by $\gam(R)$,
  that is, if $\abs{z} < R$ for all $z \in S \cap \HH$.
  For all admissible $R$, we define $M(f, R) \df \max\set{\abs{f(z)} \suchthat z \in \img(\bet(R))}$.
\end{para}

\section{Semicircle Method}

\begin{para}
  The semicircle method computes the improper integral $\int_{\ninfty}^{\pinfty} f(x) \de x$
  via computing $\flim{R}{\pinfty} \int_{\gam(R)} f(z) \de z$ for an appropriate function
  of the form described in \autoref{para:cha080:f}.
  This method does not in general yield convergence of the improper integral,
  but only its value in case of convergence.
  We need to ensure convergence first by other methods, for example by the comparison criterion.
  The following proposition gives a sufficient condition for the semicircle method to succeed.
\end{para}

\begin{proposition}
  \label{prop:cha080:semicircle-works}
  Let $f$ be as in \autoref{para:cha080:f}.
  Assume $\flim{R}{\pinfty} R \ccdot M(f, R) = 0$.
  Then:
  \begin{IEEEeqnarray*}{0l}
    \flim{R}{\pinfty} \int_{-R}^R f(x) \de x = 2 \pi \imagi \sum_{z \in S \cap \HH} \res(f,z)
  \end{IEEEeqnarray*}
\end{proposition}

\begin{proof}
  For all admissible $R$,
  we have $\int_{\gam(R)} f(z) \de z = 2 \pi \imagi \sum_{z \in S \cap \HH} \res(f,z)$
  by the Residue Theorem.
  Moreover, by assumption:
  \begin{IEEEeqnarray*}{0l+x*}
    \abs{\int_{\bet(R)} f(z) \de z} \leq L(\beta_R) \ccdot M(f, R) = \pi R \ccdot M(f, R) \tendsasp{R} 0 & \qedhere
  \end{IEEEeqnarray*}
\end{proof}

\begin{para}
  In the next proposition,
  we characterize a class of functions $f$ that fulfill the prerequisite of the preceding proposition.
  Namely those are rational functions with sufficiently large denominator
  multiplied with a sufficiently bounded function.
  For all polynomials $p \in \fnset{\CC}{\CC}$, denote $\cN(p) = \set{z \in \CC \suchthat p(z) = 0}$
  the set of its zeros.
\end{para}

\begin{proposition}
  \label{prop:cha080:semicircle-rational}
  Let $p$ and $q$ be polynomials with $\deg(q) \geq \deg(p) + 2$ and $\cN(q) \cap \RR = \emptyset$.
  Let $g \in \fnset{\Om}{\CC}$ be holomorphic and bounded on $\clHH$.
  Define $f \df \fn{z}{\Om \setminus \cN(q)}{\frac{p(z) g(z)}{q(z)}}$;
  then $f$ is as in \autoref{para:cha080:f} with $S = \cN(q)$.
  Then $\int_{\ninfty}^\pinfty f(x) \de x$ converges, and we have:
  \begin{IEEEeqnarray*}{0l}
    \int_{\ninfty}^\pinfty f(x) \de x = 2 \pi \imagi \sum_{z \in \cN(q) \cap \HH} \res(f,z)
  \end{IEEEeqnarray*}
\end{proposition}

\begin{proof}
  By \autoref{prop:cha030:poly-bounds},
  there are $a,b,C > 0$ such that for all $z \in \clHH$ we have:
  \begin{IEEEeqnarray*}{0l+l+l}
    a \ccdot \abs{z}^{\deg(q)} < \abs{q(z)}
    & \abs{p(z)} < b \ccdot \abs{z}^{\deg(p)}
    & \abs{g(z)} < C
  \end{IEEEeqnarray*}
  It follows for all $z \in \clHH \setminus \set{0}$:
  \begin{IEEEeqnarray}{0l}
    \label{eq:cha080:semicircle-rational}
    \abs{f(z)} < \frac{b \ccdot \abs{z}^{\deg(p)} \ccdot C}{a \ccdot \abs{z}^{\deg(q)}}
    \leq \frac{bC}{a} \ccdot \frac{1}{\abs{z}^2}
  \end{IEEEeqnarray}
  By the comparison criterion, $\int_{1}^\pinfty f(x) \de x$ and $\int_{\ninfty}^{-1} f(x) \de x$ converge,
  hence also $\int_{0}^\pinfty f(x) \de x$ and $\int_{\ninfty}^{0} f(x) \de x$,
  since $\int_{-1}^{1} f(x) \de x$ is just a normal integral.
  It follows that $\int_{\ninfty}^\pinfty f(x) \de x$ converges.
  From~\eqref{eq:cha080:semicircle-rational} it also follows that $\flim{R}{\pinfty} R \ccdot M(f, R) = 0$.
  The claim follows with \autoref{prop:cha080:semicircle-works}.
\end{proof}

\begin{para}
  \label{para:cha080:g-exp}
  Candidates for $g$ are, besides constant functions,
  functions of the form ${g = \fn{z}{\CC}{e^{\imagi a z}}}$ for some $a \geq 0$.
  This is since for all $z \in \clHH$ we have $\Im(z) \geq 0$,
  hence $\abs{e^{\imagi a z}} = e^{\Re(\imagi a z)} = e^{-a \ccdot \Im(z)} \leq 1$.
\end{para}

\section{Square Method}

\begin{para}
  Let $f$ be as in \autoref{para:cha080:f}.
  For all admissible $R$, define:
  \begin{IEEEeqnarray*}{0l}
    M^*(f, R) \df \sup\set{\abs{f(z)} \suchthat z \in \clHH \land \abs{z} \geq R}
  \end{IEEEeqnarray*}
  Then, clearly, $M(f,R) \leq M^*(f,R)$.
  We start with a technical remark that allows us to recognize
  how $f$ flattens around the outskirts of the complex plane --~measured by $M(f,R)$~--
  by just considering $M(f,R)$.
\end{para}

\begin{proposition}
  \label{prop:cha080:flat}
  Let $\phi \in \fnset{\RRnn}{\RRnn}$ be monotone increasing. Then:
  \begin{IEEEeqnarray*}{0l}
    \flim{R}{\pinfty} \phi(R) \ccdot M(f, R) = 0
    \implies \flim{R}{\pinfty} \phi(R) \ccdot M^*(f, R) = 0
  \end{IEEEeqnarray*}
\end{proposition}

\begin{proof}
  Assume for contradiction that $\flim{R}{\pinfty} \phi(R) \ccdot M(f, R) = 0$ holds,
  while $\flim{R}{\pinfty} \phi(R) \ccdot M^*(f, R) = 0$ does not holds.
  Then there is $\veps > 0$ and a sequence $\seq{R_n}{n}$ in $\RRnn$ with $\limx{n} R_n = \pinfty$
  and $\phi(R_n) \ccdot M^*(f, R_n) \geq \veps$ for all $n$.
  We may assume that $\seq{R_n}{n}$ is monotone increasing.
  For all $n$, by the properties of the supremum,
  there is $z_n \in \clHH$ with $\abs{z_n} \geq R_n$ such that
  $\phi(R_n) \ccdot \abs{f(z_n)} > \frac{\veps}{2}$.
  Define $R'_n \df \abs{z_n}$, then $\limx{n} R'_n = \pinfty$.
  It follows:
  \begin{IEEEeqnarray*}{0l}
    \phi(R'_n) \ccdot M(f, R'_n) \geq \phi(R_n) \ccdot M(f, R'_n)
    \geq \phi(R_n) \ccdot \abs{f(z_n)}
    > \frac{\veps}{2}
  \end{IEEEeqnarray*}
  This is a contradiction to $\flim{R}{\pinfty} \phi(R) \ccdot M(f, R) = 0$.
\end{proof}

\begin{para}
  The method presented in the next proposition makes smaller demands regarding the integrand
  than the semicircle method does regarding the behavior for $\abs{z} \tends \pinfty$;
  compare \autoref{prop:cha080:semicircle-works}.
  It moreover makes a stronger statement since it also delivers convergence of the integral.
  The price for this is that we need a factor $e^{\imagi a x}$.
\end{para}

\begin{proposition}
  Let $f$ be as in \autoref{para:cha080:f}, and assume ${\flim{R}{\pinfty} M(f, R) = 0}$.
  Let $a > 0$, and define:
  \begin{IEEEeqnarray*}{0l}
    g \df \fn{z}{\Om_{S}}{f(z) \ccdot e^{\imagi a z}}
  \end{IEEEeqnarray*}
  Then the improper integral $\int_{\ninfty}^\pinfty g(x) \de x$ converges, and its value is:
  \begin{IEEEeqnarray*}{0l}
    \int_{\ninfty}^\pinfty g(x) \de x = 2 \pi \imagi \sum_{z \in S \cap \HH} \res(g, z)
  \end{IEEEeqnarray*}
\end{proposition}

\begin{proof}
  Let $R_1,R_2 > 0$ each be admissible.
  Define $R \df R_1 + R_2$ and ${\sig \df (\sig_1, \sig_2, \sig_3, \sig_4)}$, where:
  \begin{IEEEeqnarray*}{0l+l}
    \sig_1 \df \tau(-R_1, R_2)
    &\sig_2 \df \tau(R_2, \, R_2 + \imagi R) \\
    \sig_3 \df \tau(R_2 + \imagi R, \, -R_1 + \imagi R)
    &\sig_4 \df \tau(-R_1 + \imagi R, \, -R_1)
  \end{IEEEeqnarray*}
  Then $\img(\sig)$ is a square with edge length~$R$,
  and $\sig_j$ runs along exactly one edge of this square for all $j \in \setn{4}$.
  Since $R_{1}$ and $R_{2}$ are admissible, by the Residue Theorem:
  \begin{IEEEeqnarray*}{0l}
    \int_{\sig} g(z) \de z = 2 \pi \imagi \sum_{z \in S \cap \HH} \res(g, z) \Df I
  \end{IEEEeqnarray*}
  We need estimations for the integrals along $\sig_2, \sig_3, \sig_4$.
  \par
  For $\sig_3$, that is, along the upper edge of the square, this is easy.
  By assumption and \autoref{prop:cha080:flat}, we have:
  \begin{IEEEeqnarray*}{0l}
    \abs{\int_{\sig_3} g(z) \de z}
    \leq L(\sig_3) \max_{z \in \img(\sig_3)} \abs{f(z)} \ccdot e^{-a \ccdot \Im(z)}
    \leq M^*(f,R) \ccdot R e^{-a R}
    \tendsasp{R} 0
  \end{IEEEeqnarray*}
  Hence $R_1 \tends \pinfty$ as well as $R_2 \tends \pinfty$ each effects
  that the integral along the upper edge tends to~$0$.
  \par
  Along the vertical edge on the right\-/hand side, we have:
  \begin{IEEEeqnarray*}{0l}
    \abs{\int_{\sig_2} g(z) \de z}
    \leq \int_0^1 \abs{f(\sig_2(t))} \ccdot \abs{e^{\imagi a \sig_2(t)}} \ccdot \abs{\sig_2'(t)} \, \de t \\
    \quad \leq M^*(f,R_2) \int_0^1 \abs{e^{\imagi a \sig_2(t)}} \ccdot R \, \de t
    = M^*(f,R_2) \ccdot R \int_0^1 e^{- a \ccdot \Im(\sig_2(t))} \de t \\
    \quad = M^*(f,R_2) \ccdot R \int_0^1 e^{- a \ccdot \Im(R_2 + t \imagi R)} \de t
    = M^*(f,R_2) \ccdot R \int_0^1 e^{- a R t} \de t \\
    \quad = M^*(f,R_2) \ccdot R \ccdot \fneval{\frac{-1}{a R} e^{- a R t}}{t=0}{1}
    = \frac{M^*(f,R_2)}{a} \parens{1 - e^{- a R}} \tendsasp{R_2} 0
  \end{IEEEeqnarray*}
  Likewise, we can prove $\abs{\int_{\sig_4} g(z) \de z} \tendsasp{R_1} 0$.
  It follows that if $R_1 \tends \pinfty$ \emphasis{and} $R_2 \tends \pinfty$ both happen,
  the integrals along the two vertical edges and along the upper edge each tend to~$0$.
  It remains the contribution of the edge that runs along the real axis,
  which delivers the promised value~$I$ for the improper integral, provided that it converges.\footnote{%
    In particular, we now know that $\flim{r}{\pinfty} \int_{-r}^r g(x) \de z
    = 2 \pi \imagi \sum_{z \in S \cap \HH} \res(g, z)$.
    But this is not enough for convergence of $\int_{\ninfty}^\pinfty g(x) \de x$.}
  \par
  In order to prove convergence of the improper integral,
  we have to send $R_1$ and $R_2$ to $\pinfty$ \emphasis{separately}.
  We do this for fixed $R_1$ and $R_2 \tends \pinfty$;
  the other case, where $R_1 \tends \pinfty$ while $R_2$ remains fixed, works likewise.
  \par
  We apply the Cauchy criterion (\autoref{prop:cha030:int-cauchy}).
  Let $\tiR_2 \geq R_2$ and ${\tiR \df R_1 + \tiR_2}$, and for all $j \in \setn{4}$ let $\tisig_j$ be
  like $\sig_{j}$ but with $\tiR_2$ in place of $R_2$ and $\tiR$ in place of $R$.
  Those curves hence run along the edges of a square with edge length~$\tiR$
  and lower left corner~$-R_{1}$.
  Denote $\tisig = (\tisig_1, \tisig_2, \tisig_3, \tisig_4)$.
  Clearly, we have $\abs{\int_{\tisig_2} g(z) \de z} \tendsasp{\tiR_2} 0$
  and $\abs{\int_{\tisig_3} g(z) \de z} \tendsasp{\tiR_2} 0$ by the same computation as above.
  \par
  Let $\veps > 0$ and $R_2$ large enough so that
  $\abs{\int_{\sig_j} g(z) \de z}, \, \abs{\int_{\tisig_j} g(z) \de z} < \veps$ for all $j \in \set{2,3}$.
  It follows:
  \begin{IEEEeqnarray*}{0l}
    \abs{\int_{R_2}^{\tiR_2} g(z) \de z}
    = \abs{\int_{\tisig \cat -\sig \cat - \tisig_2 \cat - \tisig_3 \cat - \tisig_4 \cat \sig_4 \cat \sig_3 \cat \sig_2} g(z)\de z}\\
    \quad = \abs{\int_{- \tisig_2 \cat - \tisig_3 \cat - \tisig_4 \cat \sig_4 \cat \sig_3 \cat \sig_2 } g(z) \de z }
    \qquad \text{since $\sig$ and $\tisig$ each give $I$} \\
    \quad < 4 \veps + \abs{\int_{- \tisig_4 \cat \sig_4} g(z) \de z}
  \end{IEEEeqnarray*}
  We are not allowed to send $R_{1}$ to $\pinfty$,
  but we achieved that only the upper part of the left edge counts now,
  namely between $-R_1 + \imagi R$ and $-R_1 + \imagi \tiR$.
  This way, we can tolerate the fixed $R_{1}$, since:
  \begin{IEEEeqnarray*}{0l}
    \abs{\int_{- \tisig_4 \cat \sig_4} g(z) \de z}
    = \abs{\int_{R}^{\tiR} g(-R_1 + \imagi t) \de t}
    \leq \int_{R}^{\tiR} \abs{f(-R_1 + \imagi t)} \ccdot e^{-a t} \de t \\
    \quad \leq M^*(f, R_1) \int_{R}^{\tiR} e^{-a t} \de t
    = \frac{M^*(f, R_1)}{a} \parens{ e^{-aR} - e^{-a\tiR} } \tendsasp{R_2} 0
  \end{IEEEeqnarray*}
  With this in hand, by making $R_{2}$ large enough,
  we can ensure $\abs{\int_{- \tisig_4 \cat \sig_4} g(z)} < \veps$.
  In total, we have shown:
  \begin{IEEEeqnarray*}{0l}
    \forall \veps > 0 \innerholds
    \exists R_0 \in \RR \innerholds
    \forall R_2, \tiR_2 \geq R_{0} \holds
    \abs{\int_{R_2}^{\tiR_2} g(x) \de x} < 5 \veps
  \end{IEEEeqnarray*}
  This is the Cauchy criterion for convergence of $\int_0^\pinfty g(x) \de x$.
\end{proof}

\begin{para}
  By \autoref{prop:cha030:poly-bounds},
  the preceding proposition is applicable in particular if
  $f$ is rational with $f=\frac{p}{q}$ where $\deg(q) \geq \deg(p) + 1$.
\end{para}

\exercisesection{cha:cha080}

\begin{exercise}
  Compute $\int_{\ninfty}^\pinfty \frac{x^2-x+2}{x^4+10x^2+9} \de x$.
\end{exercise}

\begin{exercise}
  Compute $\int_{\ninfty}^\pinfty \frac{x^2-x+2}{x^4+10x^2+9} \cos(x) \de x$.
\end{exercise}

\begin{exercise}
  Let $a,b > 0$.
  Compute $\int_{\ninfty}^{\pinfty} \frac{x e^{\imagi a x}}{x^2 + b^2} \de x$.
  \par
  From this, also compute $\int_{\ninfty}^{\pinfty} \frac{x \ccdot \sin(a x)}{x^2 + b^2} \de x$
  and $\int_0^{\pinfty} \frac{x \ccdot \sin(a x)}{x^2 + b^2} \de x$.
\end{exercise}

\solutionsection

\begin{solution}
  We start by looking at zeros of numerator and denominator.
  In the numerator, we can use \autoref{prop:cha010:abc-real}.
  We have the discriminant $(-1)^{2} - 4 \ccdot 2 = -7$,
  hence our two zeros are $\frac{1 \pm \imagi \sqrt{7}}{2}$.
  In the denominator, we have only even powers of $z$, we have for all $z \in \CC$:
  \begin{IEEEeqnarray*}{0l}
    z^4 + 10 z^2 + 9 = 0 \iff (z^{2})^4 + 10 z^2 + 9 = 0 \\
    \quad \iff z^{2} = \frac{-10\pm\sqrt{64}}{2}
    \iff z^{2} = \frac{-10\pm 8}{2}
    \iff z^{2} \in \set{-1, -9}
  \end{IEEEeqnarray*}
  It follows that the zeros of the denominator are $\pm \imagi$ and $\pm 3 \imagi$.
  Our holomorphic function is:
  \begin{IEEEeqnarray*}{0l}
    f \df \fn{z}{\CC\setminus\set{\pm \imagi, \pm 3 \imagi}}{\frac{z^2-z+2}{z^4+10z^2+9}}
  \end{IEEEeqnarray*}
  The denominator has no real zeros and has degree two more than the numerator,
  and thus the conditions of \autoref{prop:cha080:semicircle-rational} are fulfilled.
  Enumerator and denominator have no common zeros, so the zeros of the denominator are poles.
  The relevant poles (that is, those within the semicircle) are $\imagi$ and $3\imagi$
  and have order~$1$. We compute the residues:
  \begin{IEEEeqnarray*}{0l}
    \res(f,\imagi)
    = \parens{z \mapsto \frac{z^2 - z + 2}{\parens{z+\imagi} \parens{z-3\imagi} \parens{z+3\imagi} }}(\imagi)
    = \frac{1-\imagi}{(2\imagi)(-2\imagi)(4\imagi)}
    = - \frac{\imagi+1}{16}\\
    \res(f,3\imagi)
    = \parens{z \mapsto \frac{z^2 - z + 2}{\parens{z-\imagi} \parens{z+\imagi} \parens{z+3\imagi} }}(3\imagi)
    = \frac{-9-3\imagi+2}{(2\imagi)(4\imagi)(6\imagi)}
    = \frac{3-7\imagi}{48}
  \end{IEEEeqnarray*}
  It follows:
  \begin{IEEEeqnarray*}{0l}
    \int_{\ninfty}^\pinfty \frac{x^2-x+2}{x^4+10x^2+9} \de x
    = \pi \imagi \parens{- \frac{\imagi+1}{8} + \frac{3-7\imagi}{24} } \\
    \quad = \pi \imagi \parens{\frac{-3\imagi-3+ 3-7\imagi}{24} }
    = \pi \frac{5}{12}
  \end{IEEEeqnarray*}
\end{solution}
\smallskip

\begin{solution}
  Our integral is the real part of:
  \begin{IEEEeqnarray*}{0l}
    \int_{\ninfty}^\pinfty \frac{z^2-z+2}{z^4+10z^2+9} e^{\imagi z} \de z
  \end{IEEEeqnarray*}
  We can reuse most of the calculations from the previous exercise.
  Again, we apply \autoref{prop:cha080:semicircle-rational},
  now with $g \df \fn{z}{\CC}{e^{\imagi z}}$,
  which has the required properties by \autoref{para:cha080:g-exp}.
  Since $g$ has no zeros, the classification of singularities does not change,
  we still have two relevant poles of order~$1$, namely at $\imagi$ and $3\imagi$.
  For the residues, all we have to do is to multiply with $g$.
  We obtain:
  \begin{IEEEeqnarray*}{0l}
    \int_{\ninfty}^\pinfty \frac{z^2-z+2}{z^4+10z^2+9} e^{\imagi z} \de z
    = \pi \imagi \parens{\frac{(-3\imagi-3) e^{-1}+ (3-7\imagi) e^{-3}}{24} } \\
    \quad = \pi \parens{\frac{(3-3\imagi) e^{-1}+ (7+3\imagi) e^{-3}}{24} }
  \end{IEEEeqnarray*}
  It follows:
  \begin{IEEEeqnarray*}{0l}
    \int_{\ninfty}^\pinfty \frac{x^2-x+2}{x^4+10x^2+9} \cos(x) \de x
    = \pi \parens{\frac{3 e^{-1}+ 7 e^{-3}}{24}}
    \approx 0.1901
  \end{IEEEeqnarray*}
\end{solution}
\smallskip

\begin{solution}
  Clearly, the square method is applicable.
  The relevant singularity is $\imagi b$, which is a pole of order~$1$.
  We compute the residue:
  \begin{IEEEeqnarray*}{0l}
    \frac{\imagi b e^{\imagi a \imagi b}}{2\imagi b} = \frac{e^{-ab}}{2}
  \end{IEEEeqnarray*}
  It follows:
  \begin{IEEEeqnarray*}{0l}
    \int_{\ninfty}^{\pinfty} \frac{x e^{\imagi a x}}{x^2 + b^2} \de x = \pi \imagi e^{-ab}
  \end{IEEEeqnarray*}
  In particular, we have:
  \begin{IEEEeqnarray*}{0l+l}
    \int_{\ninfty}^{\pinfty} \frac{x \ccdot \sin(a x)}{x^2 + b^2} \de x = \pi e^{-ab}
    &\int_0^{\pinfty} \frac{x \ccdot \sin(a x)}{x^2 + b^2} \de x = \frac{\pi e^{-ab}}{2}
  \end{IEEEeqnarray*}
  The value of the second integral follows since the integrand is an even function.
\end{solution}

%%% Local Variables:
%%% TeX-master: "Complex_Analysis_NatAppSci.tex"
%%% End:
