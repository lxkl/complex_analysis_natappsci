f(x) = 1/(1+x^2)
def taylor(x_0, n_max):
    show([N(( abs(f.diff(n)(x_0)) / factorial(n) )^(1/n)) for n in range(n_max-20,n_max)])
taylor(0, 500)
taylor(1, 500)
