\chapter{Holomorphic Functions}
\label{cha:cha020}

\begin{para}
  Let $\Om \subseteq \CC$ be open, and let $f \in \fnset{\Om}{\CC}$.
  We call $f$ \term{complex differentiable} in $z \in \Om$ if:
  \begin{IEEEeqnarray*}{0l}
    \flim{\zeta}{z} \frac{f(\zeta) - f(z)}{\zeta - z} \in \CC
  \end{IEEEeqnarray*}
  This is equivalent to (and the limits are equal, if they exist):
  \begin{IEEEeqnarray*}{0l}
    \flim{h}{0} \frac{f(z+h) - f(z)}{h} \in \CC
  \end{IEEEeqnarray*}
  If $f$ is differentiable in $z$,
  then we denote this function limit by $f'(z)$ and call it the \term{derivative} of $f$ in $z$.
  We call $f$ \term{holomorphic} if $f$ is complex differentiable in all $z \in \Om$;
  then we have a function $f' \in \fnset{\Om}{\CC}$ called the \term{derivative} of $f$.
  A~holomorphic function with domain $\CC$ is called an \term{entire} function.
  For all open sets $U \subseteq \Om$,
  we say that $f$ is \term{holomorphic} on $U$ if $\fnres{f}{U}$ is holomorphic;
  this is the same as $f$ being complex differentiable in all points of~$U$.
\end{para}

\begin{proposition}
  \label{prop:cha020:holo-cont}
  Holomorphic functions are continuous.
\end{proposition}

\begin{proof}
  Exercise (like the proofs from real analysis
  and also like the proof of \autoref{prop:cha010:diff-cont} would be).
\end{proof}

\begin{proposition}
  \label{prop:cha020:holo-comp}
  For the combination of holomorphic functions, we have results corresponding to those
  known in the real case or as given in \autoref{prop:cha010:diff-comb}.
  That is, for all holomorphic functions $f,g \in \fnset{\Om}{\CC}$ with $\Om \subseteq \CC$ open
  and all $c \in \CC$, we have:
  \begin{IEEEeqnarray*}{0l+l+l}
    (f+g)' = f' + g' & (cf)' = c f' & (fg)' = f'g + fg'
  \end{IEEEeqnarray*}
  For all holomorphic functions $f \in \fnset{\Om}{\CC}$
  and $g \in \fnset{\Om}{\CCnz}$ with $\Om \subseteq \CC$ open, we have:
  \begin{IEEEeqnarray*}{0l}
    \parens{\frac{f}{g}}' = \frac{f'g - fg'}{g^{2}}
  \end{IEEEeqnarray*}
  For all holomorphic functions $f \in \fnset{\tiOm}{\CC}$ and $g \in \fnset{\Om}{\tiOm}$
  with ${\Om, \tiOm \subseteq \CC}$ open,
  we have the \term{chain rule} $(f \circ g)' = (f' \circ g) \ccdot g'$.
  Those results also hold point\-/wise, for example,
  if $f$ and $g$ are both complex differentiable in $z$,
  then $f+g$ is complex differentiable in $z$ with $(f+g)'(z) = f'(z) + g'(z)$.
\end{proposition}

\begin{proof}
  Exercise (like the proofs from real analysis).
\end{proof}

\begin{proposition}
  Polynomials and rational functions (with complex domain) are differentiable.
\end{proposition}

\begin{proof}
  Like the proof of \autoref{prop:cha010:rat-cont}, now using \autoref{prop:cha020:holo-comp}.
\end{proof}

\begin{proposition}
  Let $\eliz{a}{n} \in \CC$ and define:
  \begin{IEEEeqnarray*}{0l}
    f \df \fn{z}{\CC}{\sum_{j=0}^{n} a_{j} z^{j}}
  \end{IEEEeqnarray*}
  Then $f$ is holomorphic, and for all $z \in \CC$:
  \begin{IEEEeqnarray*}{0l}
    f'(z) = \sum_{j=1}^{n} j a_{j} z^{j-1} = \sum_{j=0}^{n-1} (j+1) a_{j+1} z^{j}
  \end{IEEEeqnarray*}
\end{proposition}

\begin{proof}
  Follows inductively from \autoref{prop:cha020:holo-comp}.
\end{proof}

\begin{proposition}
  \label{prop:cha020:not-holo}
  The following functions (with domain $\CC$) are \emphasis{not} holomorphic:
  $\Re$, $\Im$, and conjugation.
\end{proposition}

\begin{proof}
  We only look at $\Re$ and leave the others as exercises.
  We have:
  \begin{IEEEeqnarray*}{0l}
    \limx{n} \frac{\Re(\frac{1}{n}) - \Re(0)}{\frac{1}{n} - 0}
    = \limx{n} \frac{\Re(\frac{1}{n})}{\frac{1}{n}} = 1 \\
    \limx{n} \frac{\Re(\imagi \frac{1}{n}) - \Re(0)}{\imagi \frac{1}{n} - 0}
    = \limx{n} \frac{0}{\imagi \frac{1}{n}} = 0
  \end{IEEEeqnarray*}
  Since $\limx{n} \frac{1}{n} = 0$ and $\limx{n} \imagi \frac{1}{n} = 0$,
  the two function limits should be the same for $\Re$ to be complex differentiable in~$0$.
  However, they are different, so $\Re$ is not complex differentiable in~$0$, hence $\Re$ is not holomorphic.
\end{proof}

\begin{para}
  By \autoref{prop:cha020:holo-comp}, if $\Re(f)$ and $\Im(f)$ are each holomorphic for a function~$f$,
  then $f$ is holomorphic.
  However, the converse \emphasis{does not hold}: for example, the identity on $\CC$ is clearly holomorphic,
  but $\Re$ and $\Im$ are not.
  The reader is encouraged to compare this situation to \autoref{prop:cha010:diff-re-im}.
\end{para}

\section{Cauchy-Riemann Differential Equations}
\label{sec:cha020:CRDE}

Recall that $(\CC, \abs{\ccdot})$ can be identified in a straightforward way with
the normed vector space $(\RR^{2}, \norm{\cdot}_{2})$.
For the latter, we already have a notion of differentiability from multi\-/dimensional analysis,
which we will recall next.

\begin{para}
  Let $p,q \in \NN$.
  Let $\Om \subseteq \RR^{p}$ be open, and let $f \in \fnset{\Om}{\RR^{q}}$.
  Write $f = (\eli{f}{q})$, so $f_{j}$ is the $j$th component function of $f$ for all $j \in \setn{q}$.
  We call $f$ \term{(real) differentiable} in $v \in \Om$ if there is a matrix $A \in \RR^{q \times p}$ such that:
  \begin{IEEEeqnarray}{0l}
    \label{eq:cha020:total-diff}
    \flim{h}{0} \frac{\norm{f(v+h) - f(v) - A h}_{2}}{\norm{h}_{2}} = 0
  \end{IEEEeqnarray}
  Here, $A h$ is the product of a matrix comprising $p$ columns with a vector comprising $p$ components,
  yielding a vector with $q$ components.
  It can be proved that if $f$ is differentiable in $v$,
  then it is partially differentiable in $v$, and the matrix $A$ is uniquely determined,
  namely it is the Jacobian of $f$ in $v$, defined as:
  \begin{IEEEeqnarray*}{0l}
    J_{f}(v) \df \begin{bmatrix}
      \partial_{1} f_{1} (v) & \hdots & \partial_{p} f_{1}(v) \\
      \vdots & & \vdots \\
      \partial_{1} f_{q} (v) & \hdots & \partial_{p} f_{q}(v)
    \end{bmatrix}
  \end{IEEEeqnarray*}
  On the other hand, if $f$ is partially differentiable in $v$,
  then $J_{f}(v)$ is the only candidate for a matrix $A$ such that~\eqref{eq:cha020:total-diff} holds.
  If $J_{f}(v)$ does the job, then $f$ is differentiable in $v$; otherwise $f$ is not differentiable in $v$.
  If $f$ is partially differentiable on a neighborhood of $v$,
  and the partial derivatives (as functions defined on that neighborhood) are continuous in $v$,
  then $f$ is differentiable in $v$,
  and $J_{f}(v)$ is the uniquely determined matrix $A$ in~\eqref{eq:cha020:total-diff}.
\end{para}

\begin{para}
  We express complex differentiability in a form similar to~\eqref{eq:cha020:total-diff}.
  Let $\Om \subseteq \CC$ be open, and let $f \in \fnset{\Om}{\CC}$.
  It is easy to see that $f$ is complex differentiable in $z \in \Om$
  if and only if there is $a \in \CC$ such that:
  \begin{IEEEeqnarray}{0l}
    \label{eq:cha020:complex-diff-ah}
    \flim{h}{0} \frac{\abs{f(z+h) - f(z) - a h}}{\abs{h}} = 0
  \end{IEEEeqnarray}
  Here, $a h$ is the multiplication of two complex numbers.
  If $f$ is complex differentiable in~$z$, then $a$ is uniquely determined, namely $a = f'(z)$.
\end{para}

\begin{para}
  We make the connection between the languages of complex analysis
  and multi\-/dimensional real analysis.
  Let $\Om$ and $f$ be as in the preceding paragraph.
  We can consider $f$ also as a function defined on an open subset of $\RR^{2}$ and mapping to $\RR^{2}$.
  Namely, $f(x,y) = f(x + \imagi y)$ for all $x,y \in \RR$ such that $x + \imagi y \in \Om$.
  The functions
  $\Re f = (z \mapsto \Re(f(z)))$ and $\Im f = (z \mapsto \Im(f(z)))$
  correspond to the component functions in the language of multi\-/dimensional real analysis.
  Now let $\phi \in \fnset{\Om}{\RR}$, for example $\phi = \Re f$ or $\phi = \Im f$.
  We denote by $\partial_{\Re} \phi (z)$ the partial derivative of $\phi$ in $z \in \Om$
  with respect to the real part of the argument, that is:
  \begin{IEEEeqnarray*}{0l}
    \partial_{\Re} \phi (z)
    = \flimx{h}{0}{h \in \RR} \frac{\phi(\Re(z) + h + \imagi \ccdot \Im(z)) - \phi(z)}{h}
  \end{IEEEeqnarray*}
  Provided, of course, that this function limit exists.
  This corresponds to the partial derivative with respect to the first variable
  in the language of multi\-/dimensional real analysis.
  Likewise, we denote by $\partial_{\Im} \phi (z)$ the partial derivative of $\phi$ in $z \in \Om$
  with respect to the imaginary part of the argument;
  this corresponds to the partial derivative with respect to the second variable.
  The Jacobian $J_{f}(z)$ is of course:
  \begin{IEEEeqnarray*}{0l}
    J_{f}(z) =
    \begin{bmatrix}
      \partial_{\Re} \Re f (z) & \partial_{\Im} \Re f (z) \\
      \partial_{\Re} \Im f (z) & \partial_{\Im} \Im f (z)
    \end{bmatrix}
  \end{IEEEeqnarray*}
\end{para}

\begin{para}
  \label{para:cha020:CRDE}
  Let $\Om \subseteq \CC$ be open, and let $f \in \fnset{\Om}{\CC}$.
  Let $z \in \Om$.
  Let $f$ be real differentiable in $z$.
  Then we say that $f$ satisfies the \term{Cauchy\-/Riemann differential equations (CRDE)} in~$z$ if:
  \begin{IEEEeqnarray*}{0l}
    \partial_{\Re} \Re f (z) = \partial_{\Im} \Im f (z) \\
    \partial_{\Im} \Re f (z) = - \partial_{\Re} \Im f (z)
  \end{IEEEeqnarray*}
  In other notation, the CRDE in $z$ mean that there are $b, c \in \RR$ such that:
  \begin{IEEEeqnarray*}{0l}
    J_{f}(z) =
    \begin{bmatrix}
      b & -c \\
      c & b
    \end{bmatrix}
  \end{IEEEeqnarray*}
  We say that $f$ satisfies the CRDE if $f$ satisfies the CRDE in all $z \in \Om$.
\end{para}

\begin{proposition}
  \label{prop:cha020:CR}
  Let $\Om \subseteq \CC$ be open, and let $f \in \fnset{\Om}{\CC}$.
  Let $z \in \Om$.
  Then the following are equivalent:
  \begin{enumerate}
  \item\label{prop:cha020:CR:1} $f$ is complex differentiable in $z$.
  \item\label{prop:cha020:CR:2} $f$ is real differentiable in $z$ and $f$ satisfies the CRDE in $z$.
  \end{enumerate}
  Hence $f$ is holomorphic (that is, complex differentiable in all points of $\dom(f)$)
  if and only if $f$ is real differentiable and satisfies the CRDE.
\end{proposition}

\begin{proof}
  \impref{prop:cha020:CR:1}{prop:cha020:CR:2}
  Let $a \in \CC$ be as in~\eqref{eq:cha020:complex-diff-ah} and define:
  \begin{IEEEeqnarray*}{0l}
    A \df
    \begin{bmatrix}
      \Re(a) & - \Im(a) \\
      \Im(a) & \Re(a)
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Then $ah = Ah$ for all $h \in \CC$,
  which means $\Re(ah) = (Ah)_{1}$ and $\Im(ah) = (Ah)_{2}$.
  From~\eqref{eq:cha020:complex-diff-ah} we hence conclude that~\eqref{eq:cha020:total-diff}
  holds with $z$ in place of $v$ and with $A$ as defined above;
  note that $\norm{(x,y)}_{2} = \abs{x + \imagi y}$ for all $x,y \in \RR$.
  Hence $f$ is real differentiable in $z$ with $J_{f}(z) = A$.
  The latter implies the CRDE due to the form of~$A$.
  \par
  \impref{prop:cha020:CR:2}{prop:cha020:CR:1}
  Define $a \df \partial_{\Re} \Re f (z) + \imagi \ccdot \partial_{\Re} \Im f (z)$.
  Then $\Re(ah) = (J_{f}(z) \, h)_{1}$ and $\Im(ah) = (J_{f}(z) \, h)_{2}$ for all $h \in \CC$.
  Hence~\eqref{eq:cha020:total-diff} with $z$ in place of $v$ implies~\eqref{eq:cha020:complex-diff-ah}.
\end{proof}

\begin{para}
  \label{para:cha020:f'=a}
  In the second implication in the above proof, we have:
  \begin{IEEEeqnarray*}{0rCl}
    f'(z) = a &=& \partial_{\Re} \Re f (z) + \imagi \ccdot \partial_{\Re} \Im f (z) \\
    &=& \partial_{\Re} \Re f (z) - \imagi \ccdot \partial_{\Im} \Re f (z) \\
    &=& \partial_{\Im} \Im f (z) + \imagi \ccdot \partial_{\Re} \Im f (z) \\
    &=& \partial_{\Im} \Im f (z) - \imagi \ccdot \partial_{\Im} \Re f (z)
  \end{IEEEeqnarray*}
  Or, using $b,c \in \RR$ as in \autoref{para:cha020:CRDE},
  we have $f'(z) = b + \imagi c$.
  Hence if $f$ is complex differentiable in $z$,
  the derivative $f'(z)$ is already determined by $\Re f$ alone
  and also by $\Im f$ alone.
\end{para}

\begin{example}
  The conjugation has the following Jacobian at all points of $\CC$:
  \begin{IEEEeqnarray*}{0l}
    \begin{bmatrix}
      1 & 0 \\
      0 & -1
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Hence the conjugation is not complex differentiable anywhere,
  hence in particular it is not holomorphic (as was already stated in \autoref{prop:cha020:not-holo}).
\end{example}

\section{Power Series are Holomorphic}
\label{sec:cha020:power-series}

\begin{para}
  Let $\Om \subseteq \CC$ be open, and let $f \in \fnset{\Om}{\CC}$.
  If $f$ is holomorphic, we can look at $f'$ and determine if it is also holomorphic.
  If so, we can look at its derivative, denoted by $f''$ and called the \term{second derivative} of $f$.
  Continuing in this manner, we obtain the notion of \term{$k$th derivative} for all $k \in \NN$,
  denoted by $f^{(k)}$.
  We also write $f = f^{(0)}$ and $f' = f^{(1)}$ and $f'' = f^{(2)}$.
  Later, we will learn that all holomorphic functions admit a $k$th derivative, for all $k \in \NN$.
  In this section, we will prove that power series are holomorphic
  and admit a $k$th derivative for all $k$.
  We start with some technical preparations.
\end{para}

\begin{proposition}
  \label{prop:cha020:sqrt-n}
  Let $\seq{x_{n}}{n}$ be a sequence in $\RRnn$. We have:
  \begin{enumerate}
  \item $\limx{n} \sqrt[n]{n} = 1$
  \item If $\limx{n} x_{n} > 0$, then $\limx{n} \sqrt[n]{x_{n}} = 1$.
    (This does not hold in general if ${\limx{n} x_{n} = 0}$.)
  \item Let $k \in \NN$.
    Then $\limsupx{n} \sqrt[n]{x_{n}} = \limsupx{n} \sqrt[n]{x_{n+k}}$.
  \end{enumerate}
\end{proposition}

\begin{proof}
  The first two items are standard in real analysis.
  We only look at the third item.
  Clearly, it suffices to show that:
  \begin{IEEEeqnarray*}{0l}
    \limsupx{n} \sqrt[n+k]{x_{n+k}} = \limsupx{n} \sqrt[n]{x_{n+k}}
  \end{IEEEeqnarray*}
  For all $m \in \NN$, we have:
  \begin{IEEEeqnarray*}{0l}
    \sqrt[m]{x_{m+k}}
    = \parens{\sqrt[m+k]{x_{m+k}}}^{1+\frac{k}{m}}
    = \sqrt[m+k]{x_{m+k}} \ccdot \sqrt[m]{\parens{\sqrt[m+k]{x_{m+k}}}^{k}} \\
    \sqrt[m+k]{x_{m+k}}
    = \parens{\sqrt[m]{x_{m+k}}}^{1-\frac{k}{m+k}}
    =
    \begin{cases}
      0 & \text{if $x_{m+k} = 0$} \\
      \sqrt[m]{x_{m+k}} \ccdot \sqrt[m+k]{\parens{\sqrt[m]{x_{m+k}}}^{-k}} & \text{otherwise}
    \end{cases}
  \end{IEEEeqnarray*}
  From this and using the second item, it is easy to conclude
  that for all ${\sig \in \fnset{\NN}{\NN}}$ where $\sig$ is strictly increasing, we have:
  \begin{IEEEeqnarray*}{0l}
    \limx{n} \sqrt[\sig(n)+k]{x_{\sig(n)+k}} \in \RRnn \cup \set{\pinfty}
    \iff \limx{n} \sqrt[\sig(n)]{x_{\sig(n)+k}} \in \RRnn \cup \set{\pinfty} \comma
  \end{IEEEeqnarray*}
  and that if so, then both limits are equal.
  The claim follows from this and the definition of $\limsup$.
\end{proof}

\begin{para}
  For all $n, k \in \NNzero$ with $k \leq n$, we define the \term{falling factorial}:
  \begin{IEEEeqnarray*}{0l}
    (n)_k \df \prod_{i=0}^{k-1} (n-i) = n \ccdot (n-1) \ccdot \hdots \ccdot (n-(k-1))
  \end{IEEEeqnarray*}
  In particular, $(n)_0 = 1$ and $(n)_1 = n$ and $(n)_n = n!$.
\end{para}

\begin{proposition}
  \label{prop:cha020:power-holo}
  Let $a = \seqzero{a_n}{n}$ be a sequence in $\CC$ such that $R \df R(a) \neq 0$,
  and let $z_0 \in \CC$.
  Then
  \begin{IEEEeqnarray*}{0l}
    f \df \fn{z}{D(z_0, R)}{\sum_{n=0}^\infty a_n (z - z_0)^n}
  \end{IEEEeqnarray*}
  defines a holomorphic function. Its derivative in $z \in D(z_0, R)$ is:
  \begin{IEEEeqnarray*}{0l}
    f'(z) = \sum_{n=1}^\infty a_n n (z - z_0)^{n-1}
    = \sum_{n=0}^\infty a_{n+1} (n+1) (z - z_0)^{n}
  \end{IEEEeqnarray*}
  Moreover, $f$ admits its $k$th derivative for all $k \in \NNzero$, which is:
  \begin{IEEEeqnarray*}{0l}
    f^{(k)}(z) = \sum_{n=k}^\infty a_n (n)_k (z - z_0)^{n-k}
  \end{IEEEeqnarray*}
  In particular, $f^{(k)}(z_0) = a_k k!$.
  \par
  Stated shortly: each power series defines a holomorphic function
  within its radius of convergence, and its derivative can be computed element\-/wise
  (that is, computing the derivative of each addend $a_n (z - z_0)^n$ in the series).
\end{proposition}

\begin{proof}
  Let $z \in D(z_0, R)$ and $h \in \CCnz$ such that $z + h \in D(z_0, R)$.
  Denote $w \df z - z_0$. Then:
  \begin{IEEEeqnarray*}{0l}
    \frac{f(z + h) - f(z)}{h}
    = \frac{\sum_{n=0}^\infty a_n (z + h - z_0)^n - \sum_{n=0}^\infty a_n (z - z_0)^n}{h} \\
    \quad = \limx{N} \frac{\sum_{n=0}^N a_n (w + h)^n - \sum_{n=0}^N a_n w^n}{h} \\
    \quad = \limx{N} \frac{\sum_{n=0}^N a_n ((w + h)^n - w^n)}{h} \\
    \quad = \limx{N}
    \frac{\sum_{n=0}^N a_n \parens{\parens{\sum_{t=0}^n \binom{n}{t} w^{n-t} h^t} - w^n}}{h} \\
    \quad = \limx{N} \frac{\sum_{n=1}^N a_n \sum_{t=1}^n \binom{n}{t} w^{n-t} h^t}{h} \\
    \quad = \limx{N} \sum_{n=1}^N a_n \sum_{t=1}^n \binom{n}{t} w^{n-t} h^{t-1} \\
    \quad = \limx{N} \sum_{n=1}^N a_n
    \parens{\binom{n}{1} w^{n-1} + \sum_{t=2}^n \binom{n}{t} w^{n-t} h^{t-1}} \\
    \quad = \limx{N}
    \parens{\sum_{n=0}^{N-1} a_{n+1} (n+1) w^{n} + h \sum_{n=2}^N a_n \sum_{t=2}^n \binom{n}{t} w^{n-t} h^{t-2}}
  \end{IEEEeqnarray*}
  We examine the following two sequences:
  \begin{IEEEeqnarray}{0l}
    \seq{\sum_{n=0}^{N-1} a_{n+1} (n+1) w^{n}}{N} \label{eq:cha020:power-series-1} \\
    \seq{h \sum_{n=2}^N a_n \sum_{t=2}^n \binom{n}{t} w^{n-t} h^{t-2}}{N} \label{eq:cha020:power-series-2}
  \end{IEEEeqnarray}
  The first sequence converges for the following reasons.
  It is a power series with coefficients $\seqzero{a_{n+1} (n+1)}{n}$ and center~$0$.
  By \autoref{prop:cha020:sqrt-n}, we have:
  \begin{IEEEeqnarray*}{0l}
    \limsupx{n} \sqrt[n]{\abs{a_{n+1}} (n+1)}
    = \limsupx{n} \sqrt[n]{\abs{a_{n}}} \ccdot \sqrt[n]{n}
    = \limsupx{n} \sqrt[n]{\abs{a_{n}}}
  \end{IEEEeqnarray*}
  The last equation holds since $\limx{n} \sqrt[n]{n} = 1$.
  Hence the power series from~\eqref{eq:cha020:power-series-1} has radius of convergence $R$,
  just like $\sum_{n=0}^\infty a_n (z - z_0)^n$.
  By definition of $w$ and choice of $z$, we have $w \in D(0, R)$,
  hence~\eqref{eq:cha020:power-series-1} converges.
  It follows that~\eqref{eq:cha020:power-series-2} also converges,
  since the sum of~\eqref{eq:cha020:power-series-1} and~\eqref{eq:cha020:power-series-2} converges,
  namely to $\frac{f(z + h) - f(z)}{h}$ (recall that $h$ is fixed at the moment).
  \par
  It follows:
  \begin{IEEEeqnarray}{0l}
    \label{eq:cha020:power-holo:1}
    \frac{f(z + h) - f(z)}{h}
    = \sum_{n=0}^\infty a_{n+1} (n+1) w^{n} + h \sum_{n=2}^\infty a_n \sum_{t=2}^n \binom{n}{t} w^{n-t} h^{t-2}
  \end{IEEEeqnarray}
  Suppose for a moment that we knew that
  \begin{IEEEeqnarray}{0l}
    \label{eq:cha020:power-holo:2}
    \sum_{n=2}^\infty a_n \sum_{t=2}^n \binom{n}{t} z^{n-t} h^{t-2}
  \end{IEEEeqnarray}
  as a function of $h$ was bounded on a neighborhood of $0$.
  Then we are done, since then the whole right\-/hand side in~\eqref{eq:cha020:power-holo:1}
  tends to $\sum_{n=1}^\infty a_n n z^{n-1}$ for $h \tends 0$.
  This yields complex differentiability of $f$ in $z$ and also the claimed expression
  of $f'(z) = \sum_{n=0}^\infty a_{n+1} (n+1) (z-z_{0})^{n}$.
  \par
  Hence we concentrate on~\eqref{eq:cha020:power-holo:2} now.
  We have for all $0 \leq t \leq n-2$:
  \begin{IEEEeqnarray*}{0l}
    \binom{n}{t+2} = \binom{n-2}{t} \frac{n (n-1)}{(t+2)(t+1)} < \binom{n-2}{t} n (n-1)
  \end{IEEEeqnarray*}
  It follows:
  \begin{IEEEeqnarray*}{0l}
    \abs{\sum_{n=2}^\infty a_n \sum_{t=2}^n \binom{n}{t} w^{n-t} h^{t-2}}
    \leq \sum_{n=2}^\infty \abs{a_n} \sum_{t=2}^n \binom{n}{t} \abs{w}^{n-t} \abs{h}^{t-2} \\
    \quad = \sum_{n=2}^\infty \abs{a_n} \sum_{t=0}^{n-2} \binom{n}{t+2} \abs{w}^{n-2-t} \abs{h}^{t} \\
    \quad \leq \sum_{n=2}^\infty \abs{a_n} n(n-1) \sum_{t=0}^{n-2} \binom{n-2}{t} \abs{w}^{n-2-t} \abs{h}^{t} \\
    \quad = \sum_{n=2}^\infty \abs{a_n} n(n-1) \parens{\abs{w} + \abs{h}}^{n-2}
    = \sum_{n=0}^\infty \abs{a_{n+2}} (n+2)(n+1) \parens{\abs{w} + \abs{h}}^n
  \end{IEEEeqnarray*}
  The last object is a power series with coefficients $\seq{\abs{a_{n+2}} (n+2)(n+1)}{n}$.
  By \autoref{prop:cha020:sqrt-n}, we have like before:
  \begin{IEEEeqnarray*}{0l}
    \limsupx{n} \sqrt[n]{\abs{a_{n+2}} (n+2)(n+1)}
    = \limsupx{n} \sqrt[n]{\abs{a_{n}}} \ccdot \sqrt[n]{n} \ccdot \sqrt[n]{n-1}
    = \limsupx{n} \sqrt[n]{\abs{a_{n}}}
  \end{IEEEeqnarray*}
  It follows that this last power series also has $R$ as its radius of convergence.
  Since $\abs{w} < R$, we have $\abs{w} + \abs{h} < R$ for $h$ close enough to~$0$,
  so this power series converges for all such $h$.
  The corresponding limits are monotone decreasing in $\abs{h}$.
  It follows the desired boundedness.
  \par
  In total, we have proved that $f$ is holomorphic and that the claimed expression for $f'$ holds.
  As a power series, $f'$ is again holomorphic.
  The remaining statements follow by induction.
\end{proof}

\begin{proposition}
  The functions $\exp$, $\cos$, and $\sin$ are entire functions.
  Moreover, we have $\exp' = \exp$ and $\cos' = -\sin$ and $\sin' = \cos$.
\end{proposition}

\begin{proof}
  Since $\exp$ is defined via a power series, it is holomorphic by \autoref{prop:cha020:power-holo}.
  The functions $\cos$ and $\sin$ are holomorphic as combinations of holomorphic functions.
  Also by \autoref{prop:cha020:power-holo}, for all $z \in \CC$, we have:
  \begin{IEEEeqnarray*}{0l}
    \exp'(z) = \sum_{n=1}^{\infty} \frac{z^{n-1} n}{n!}
    = \sum_{n=1}^{\infty} \frac{z^{n-1}}{(n-1)!}
    = \sum_{n=0}^{\infty} \frac{z^{n}}{n!}
    = \exp(z)
  \end{IEEEeqnarray*}
  The formulas for $\cos'$ and $\sin'$ now follow from an easy computation from the definitions.
\end{proof}

In a later chapter, we will give a version of the \term{identity theorem} for holomorphic functions.
Here we give a version just for power series.

\begin{proposition}[Identity Theorem for Power Series]
  \label{prop:cha020:id}
  Let $a = \seqzero{a_{n}}{n}$ and $b = \seqzero{b_{n}}{n}$ be sequences in $\CC$,
  and let $z_{0} \in \CC$.
  Define $\Om \df D(z_0, \min\set{R(a), R(b)})$ and:
  \begin{IEEEeqnarray*}{0l+l}
    f \df \fn{z}{\Om}{\sum_{n=0}^\infty a_n (z-z_0)^n} &
    g \df \fn{z}{\Om}{\sum_{n=0}^\infty b_n (z-z_0)^n}
  \end{IEEEeqnarray*}
  Assume that there is a sequence $\seq{z_{n}}{n}$ in $\Om \setminus \set{z_0}$ such that $\limx{n} z_n = z_0$
  and $f(z_n) = g(z_n)$ for all $n \in \NN$.
  \par
  Then $a=b$, that is, $a_n = b_n$ for all $n \in \NNzero$.
\end{proposition}

\begin{proof}
  We first prove the following claim:
  let $c = \seqzero{c_{n}}{n}$ be a sequence in~$\CC$,
  such that there is $k \in \NNzero$ with $c_{k} \neq 0$.
  Define:
  \begin{IEEEeqnarray*}{0l}
    h \df \fn{z}{D(z_0, R(c))}{\sum_{n=0}^\infty c_n (z-z_0)^n}
  \end{IEEEeqnarray*}
  Then there is an open set $V \subseteq D(z_0, R(c))$
  such that $z_0 \in V$ and $f(z) \neq 0$ for all $z \in V \setminus \set{z_0}$.
  \par
  Let $k \in \NNzero$ be minimal with $c_k \neq 0$.
  Define:
  \begin{IEEEeqnarray*}{0l}
    \tih \df \fn{z}{D(z_0, R(c))}{\sum_{n=0}^\infty c_{n+k} (z-z_0)^n}
  \end{IEEEeqnarray*}
  Then $h(z) = (z-z_0)^k \ccdot \tih(z)$ for all $z \in D(z_0, R(c))$.
  The function $\tih$ is holomorphic and in particular continuous.
  Due to continuity and $\tih(z_0) = a_k \neq 0$
  there is an open set $V \subseteq D(z_0, R(c))$ with $z_0 \in V$ such that
  $\tih(z) \neq 0$ for all $z \in V$.
  Since $(z-z_0)^k \neq 0$ for all $z \neq z_0$,
  it follows that $f(z) \neq 0$ for all $z \in V \setminus \set{z_0}$.
  This concludes the proof of the claim.
  \par
  Now define $h \df f - g$.
  Then $h(z) = \sum_{n=0}^\infty (a_n - b_n) (z-z_0)^n$ for all $z \in \Om$.
  For contradiction, assume there is $k \in \NNzero$ such that $a_k \neq b_k$.
  By the claim, there is an open set $U \subseteq \Om$ with $z_0 \in V$ such that
  $h(z) \neq 0$ for all $z \in U \setminus \set{z_0}$.
  Since $U$ is open and by the convergence of $\seq{z_{n}}{n}$,
  there is $n_0 \in \NN$ such that $z_n \in U$ for all $n \geq n_0$.
  For such $n$ we have $h(z_n) = 0$ on the one hand due to the assumptions of the proposition,
  and $h(z_n) \neq 0$ on the other hand due to $z_n \in U \setminus \set{z_0}$; a contradiction.
\end{proof}

%%%
%%% Exercises
%%%

\exercisesection{cha:cha020}

\begin{exercise}
  We know that $f \df \fn{z}{\CC}{z^{2}}$ is holomorphic.
  Verify this by checking the CRDE.
\end{exercise}

\begin{exercise}
  Determine in which points the following functions defined on $\CC$ are complex differentiable:
  $\Re$, $\Im$, and $\fn{z}{\CC}{\abs{z}}$.
\end{exercise}

\begin{exercise}
  Determine for which $n, k \in \NN$ the function $\fn{x + \imagi y}{\CC}{x^{n} + \imagi y^{k}}$
  is holomorphic.
  Note that in a different notation, this function is:
  \begin{IEEEeqnarray*}{0l}
    \fn{z}{\CC}{(\Re(z))^{n} + \imagi \ccdot (\Im(z))^{k}}
  \end{IEEEeqnarray*}
\end{exercise}

\begin{exercise}
  We know from \autoref{sec:cha020:power-series} that $\exp$ is holomorphic with $\exp' = \exp$.
  This can also be proved using only \autoref{sec:cha020:CRDE} and knowledge from real analysis,
  as in \autoref{exc:cha010:derivative-exp}.
\end{exercise}

\begin{exercise}
  Let $\Om \subseteq \CC$ be open, and let $f \in \fnset{\Om}{\CC}$.
  Provided the relevant partial derivatives exist,
  define the \term{Wirtinger derivatives} of $f$ in $z \in \Om$:
  \begin{IEEEeqnarray*}{0l+l}
    f_z(z) \df \frac{\partial_\Re f(z) - \imagi \ccdot \partial_\Im f(z)}{2} &
    f_{\conj{z}}(z) \df \frac{\partial_\Re f(z) + \imagi \ccdot \partial_\Im f(z)}{2}
  \end{IEEEeqnarray*}
  By $\partial_{\Re} f$, we mean $\partial_{\Re} \Re f + \imagi \ccdot \partial_{\Re} \Im f$,
  and likewise for $\partial_{\Im} f$.
  The \enquote{$z$} and the \enquote{$\bar{z}$} in the index is to be understood as a symbolic notation
  with no connection to the variable~$z$.
  \par
  Prove that the following are equivalent:
  \begin{enumerate}
  \item $f$ is complex differentiable in $z$.
  \item $f$ is real differentiable in $z$ and $f_{\conj{z}}(z) = 0$.
  \end{enumerate}
  Moreover, prove that if one --~and hence both~-- of these condition hold, we have $f'(z) = f_z(z)$.
\end{exercise}

\begin{exercise}
  Let $\Om \subseteq \CC$ be open, and let $f \in \fnset{\Om}{\CC}$ be holomorphic.
  Define $\conj{\Om} \df \set{\conj{z} \suchthat z \in \Om}$.
  Prove that $g \df \fn{z}{\conj{\Om}}{\conj{f(\conj{z})}}$ is holomorphic
  and $g'(z) \in \conj{f'(\conj{z})}$ for all $z \in \conj{\Om}$.
\end{exercise}

\begin{exercise}
  We already know $\cos' = -\sin$ and $\sin' = \cos$.
  Prove this by using \autoref{prop:cha010:trig-series} and \autoref{prop:cha020:power-holo}.
\end{exercise}

\begin{exercise}
  Let $a = \seqzero{a_{n}}{n}$ and $b = \seqzero{b_{n}}{n}$ be sequences in $\CC$,
  and let $z_{0} \in \CC$.
  Define $\Om \df D(z_0, \min\set{R(a), R(b)})$ and:
  \begin{IEEEeqnarray*}{0l+l}
    f \df \fn{z}{\Om}{\sum_{n=0}^\infty a_n (z-z_0)^n} &
    g \df \fn{z}{\Om}{\sum_{n=0}^\infty b_n (z-z_0)^n}
  \end{IEEEeqnarray*}
  Prove that $f+g$ and $fg$ can be written as a power series.
\end{exercise}

\begin{exercise}
  Prove $(\cos(z))^{2} + (\sin(z))^{2} = 1$ for all $z \in \CC$
  using the Identity Theorem for Power Series.
\end{exercise}

\begin{exercise}
  \label{exc:cha020:derive-inverse}
  Let $\Om \subseteq \CC$ be open, and let $f \in \fnset{\Om}{\CC}$ be injective.
  Let $z \in \Om$ such that $f$ is complex differentiable in $z$ with $f'(z) \neq 0$,
  and denote $w \df f(z)$.
  \begin{enumerate}
  \item Make the additional assumption that $\img(f)$ is open and $f^{-1}$ is continuous in $w$.
    Prove that $f^{-1}$ is complex differentiable in $w$, and we have:
    \begin{IEEEeqnarray*}{0l}
      (f^{-1})'(w) = \frac{1}{f'(z)} = \frac{1}{f'(f^{-1}(w))}
    \end{IEEEeqnarray*}
  \item Can you prove the same, but not assuming $f^{-1}$ being continuous,
    but instead assuming that
    $f$ is an \term{open map}, that is, $\fnimg{f}(A)$ is open for all open sets $A \subseteq \Om$?
  \end{enumerate}
\end{exercise}

%%%
%%% Solutions
%%%

\solutionsection

\begin{solution}
  For all $x + \imagi y \in \CC$, we have:
  \begin{IEEEeqnarray*}{0l}
    f(x + \imagi y) = x^{2} - y^{2} + \imagi \ccdot 2 xy
  \end{IEEEeqnarray*}
  It follows:
  \begin{IEEEeqnarray*}{0l}
    J_{f}(x+\imagi y)
    \begin{bmatrix}
      2x & -2y \\
      2y & 2x
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Hence the CRDE are fulfilled.
\end{solution}

\begin{solution}
  The Jacobian in $x + \imagi y \in \CC$ for $\Re$ and $\Im$ are:
  \begin{IEEEeqnarray*}{0l+l}
    \begin{bmatrix}
      1 & 0 \\
      0 & 0
    \end{bmatrix}
    &
    \begin{bmatrix}
      0 & 0 \\
      0 & 1
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Hence $\Re$ and $\Im$ are not complex differentiable in any point.
  The absolute value is not even real differentiable in~$0$.
  In all $z = x + \imagi y \in \CCnz$, we have its Jacobian:
  \begin{IEEEeqnarray*}{0l+l}
    \begin{bmatrix}
      \frac{x}{\abs{x + \imagi y}} & \frac{y}{\abs{x + \imagi y}} \\
      0 & 0
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Hence the absolute value is not complex differentiable in any point.
\end{solution}

\begin{solution}
  For all $x + \imagi y \in \CC$, we have:
  \begin{IEEEeqnarray*}{0l}
    J_{f}(x+\imagi y) =
    \begin{bmatrix}
      n x^{n-1} & 0 \\
      0 & k y^{k-1}
    \end{bmatrix}
  \end{IEEEeqnarray*}
  In particular:
  \begin{IEEEeqnarray*}{0l}
    J_{f}(1+\imagi \ccdot 1) =
    \begin{bmatrix}
      n & 0 \\
      0 & k
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Hence the CRDE can at most be fulfilled if $n=k$.
  Moreover, if $n=k$, we have:
  \begin{IEEEeqnarray*}{0l}
    J_{f}(1+\imagi \ccdot 2) =
    \begin{bmatrix}
      n & 0 \\
      0 & n 2^{n-1}
    \end{bmatrix}
  \end{IEEEeqnarray*}
  Hence the only case where the CRDE are fulfilled is $n=k=1$.
\end{solution}

\begin{solution}
  By Euler's formula, for all $x + \imagi y \in \CC$ we have:
  \begin{IEEEeqnarray*}{0l}
    \exp(x + \imagi y) = e^{x} \cos(y) + \imagi e^{x} \sin(y)
  \end{IEEEeqnarray*}
  This function is real differentiable with:
  \begin{IEEEeqnarray*}{0l}
    J_{\exp}(x + \imagi y) =
    \begin{bmatrix}
      e^{x} \cos(y) & - e^{x} \sin(y) \\
      e^{x} \sin(y) & e^{x} \cos(y)
    \end{bmatrix}
  \end{IEEEeqnarray*}
  The CRDE are hence fulfilled, so $\exp$ is holomorphic.
  By \autoref{para:cha020:f'=a}, we have:
  \begin{IEEEeqnarray*}{0l}
    \exp'(x + \imagi y) = e^{x} \cos(y) + \imagi e^{x} \sin(y) = \exp(x + \imagi y)
  \end{IEEEeqnarray*}
\end{solution}

\begin{solution}
  We know that the following conditions are equivalent:
  \begin{enumerate}[label=(\roman*)]
  \item $f$ is complex differentiable in $z$.
  \item $f$ is real differentiable in $z$ and $f$ satisfies the CRDE in $z$.
  \end{enumerate}
  Hence it suffices to show that for a real differentiable (and hence partially differentiable) function $f$,
  the CRDE in $z$ are equivalent to $f_{\bar{z}}(z) = 0$.
  A complex number is~$0$ if and only if its real part and its imaginary part are both~$0$.
  We have:
  \begin{IEEEeqnarray*}{0l}
    f_{\bar{z}}(z) = 0
    \iff \partial_{\Re} f(z) + \imagi \ccdot \partial_{\Im} f(z) = 0 \\
    \quad \iff \parens{\partial_{\Re} \Re f(z) + \imagi \ccdot \partial_{\Re} \Im f(z)}
    + \imagi \ccdot \parens{\partial_{\Im} \Re f(z) + \imagi \ccdot \partial_{\Im} \Im f(z)} = 0 \\
    \quad \iff \parens{\partial_{\Re} \Re f(z) - \partial_{\Im} \Im f(z)}
    + \imagi \ccdot \parens{\partial_{\Re} \Im f(z) + \partial_{\Im} \Re f(z)} = 0 \\
    \quad \iff \parens{\partial_{\Re} \Re f(z) - \partial_{\Im} \Im f(z) = 0}
    \land \parens{\partial_{\Re} \Im f(z) + \partial_{\Im} \Re f(z) = 0} \\
    \quad \iff \parens{\partial_{\Re} \Re f(z) = \partial_{\Im} \Im f(z)}
    \land \parens{\partial_{\Re} \Im f(z) = - \partial_{\Im} \Re f(z)} \\
    \iff \text{CRDE in $z$}
  \end{IEEEeqnarray*}
  This concludes the proof for the equivalence.
  Moreover:
  \begin{IEEEeqnarray*}{0l}
    f_{z}(z) = \partial_\Re f(z) - \imagi \ccdot \partial_\Im f(z) \\
    \quad = \partial_\Re \Re f(z) + \imagi \ccdot \partial_\Re \Im f(z)
    - \imagi \ccdot \partial_\Im \Re f(z) + \partial_\Im \Im f(z) \\
    \quad = \partial_\Re \Re f(z) + \imagi \ccdot \partial_\Re \Im f(z)
    + \imagi \ccdot \partial_\Re \Im f(z) + \partial_\Re \Re f(z) \qquad \text{by CRDE} \\
    \quad = 2 \parens{\partial_\Re \Re f(z) + \imagi \ccdot \partial_\Re \Im f(z)} \\
    \quad = 2 f'(z) \qquad \text{by \autoref{para:cha020:f'=a}}
  \end{IEEEeqnarray*}
\end{solution}

\begin{solution}
  We have for all $\zeta, z \in \conj{\Om}$ with $\zeta \neq z$:
  \begin{IEEEeqnarray*}{0l}
    \frac{g(\zeta) - g(z)}{\zeta - z}
    = \frac{\conj{f(\conj{\zeta})} - \conj{f(\conj{z})}}{\zeta - z}
    = \frac{\conj{f(\conj{\zeta}) - f(\conj{z})}}{\zeta - z}
    = \conj{\parens{\frac{\conj{\conj{f(\conj{\zeta}) - f(\conj{z})}}}{\conj{\zeta - z}}}}
    = \conj{\parens{\frac{f(\conj{\zeta}) - f(\conj{z})}{\conj{\zeta} - \conj{z}}}}
  \end{IEEEeqnarray*}
  Since conjugation is continuous, we have:
  \begin{IEEEeqnarray*}{0l}
    \flim{\zeta}{z} \frac{g(\zeta) - g(z)}{\zeta - z}
    = \conj{\flim{\zeta}{z} \frac{f(\conj{\zeta}) - f(\conj{z})}{\conj{\zeta} - \conj{z}}}
    = \conj{f'(\conj{z})}
  \end{IEEEeqnarray*}
  The last step is true since $f$ is holomorphic
  and since if a sequence in $\conj{\Om}$ tends to $z \in \conj{\Om}$,
  then its conjugation is a sequence in $\Om$ which tends to $\conj{z} \in \Om$.
\end{solution}

\begin{solution}
  Let $z \in \CC$. We have:
  \begin{IEEEeqnarray*}{0l}
    \cos'(z) = \sum_{n=1}^\infty \frac{(-1)^n \ccdot 2n}{(2n)!} z^{2n-1} \\
    \quad = \sum_{n=1}^\infty \frac{(-1)^n}{(2n-1)!} z^{2n-1} \\
    \quad = \sum_{n=0}^\infty \frac{(-1)^{n+1}}{(2(n+1)-1)!} z^{2(n+1)-1} \\
    \quad = \sum_{n=0}^\infty \frac{(-1)^{n+1}}{(2n+2-1)!} z^{2n+2-1} \\
    \quad = \sum_{n=0}^\infty \frac{(-1)^{n+1}}{(2n+1)!} z^{2n+1} \\
    \quad = - \sum_{n=0}^\infty \frac{(-1)^{n}}{(2n+1)!} z^{2n+1} = - \sin(z)
  \end{IEEEeqnarray*}
  Likewise:
  \begin{IEEEeqnarray*}{0l}
    \sin'(z) = \sum_{n=0}^\infty \frac{(-1)^n (2n+1)}{(2n+1)!} z^{2n}
    = \sum_{n=0}^\infty \frac{(-1)^n}{(2n)!} z^{2n}
  \end{IEEEeqnarray*}
  Note that we start at $n=0$ since the first exponent in the series for $\sin(z)$ is $2n+1$.
\end{solution}

\begin{solution}
  For $f+g$, the claim follows from the basic properties of the limit.
  For $fg$, we use the Cauchy product to obtain for all $z \in \Om$:
  \begin{IEEEeqnarray*}{0l}
    f(z) g(z) = \sum_{n=0}^\infty a_n (z-z_0)^n \sum_{n=0}^\infty b_n (z-z_0)^n \\
    \quad = \sum_{n=0}^{\infty} \sum_{k=0}^n a_k (z-z_0)^k b_{n-k} (z-z_0)^{n-k} \\
    \quad = \sum_{n=0}^{\infty} \sum_{k=0}^n a_k b_{n-k} (z-z_0)^{n}
    = \sum_{n=0}^{\infty} \parens{\sum_{k=0}^n a_k b_{n-k}} (z-z_0)^{n}
  \end{IEEEeqnarray*}
  So we have a power series with coefficients $\seqzero{\sum_{k=0}^n a_k b_{n-k}}{n}$.
\end{solution}

\begin{solution}
  By the previous exercise, the function
  \begin{IEEEeqnarray*}{0l}
    f \df \fn{z}{\CC}{(\cos(z))^{2} + (\sin(z))^{2} - 1}
  \end{IEEEeqnarray*}
  can be written as a power series.
  Hence we can apply the Identity Theorem for Power Series.
  By \autoref{prop:cha010:trig-basic}, we have $f(x) = 0$ for all $x \in \RR$.
  In particular, $f(\frac{1}{n}) = 0$ for all $n \in \NN$.
  By the Identity Theorem, we obtain $f(z) = 0$ for all $z \in \CC$, which proves the claim.
\end{solution}

\begin{solution}
  \begin{enumerate}
  \item Denote $V \df \img(f)$.
    Let $\seq{b_{n}}{n}$ be a sequence in $V \setminus \set{w}$ with ${\limx{n} b_{n} = w}$.
    Define $a_{n} \df f^{-1}(b_{n})$ for all $n \in \NN$.
    Since $f^{-1}$ is continuous in $w$, we have $\limx{n} a_{n} = \limx{n} f^{-1}(b_{n}) = f^{-1}(w) = z$.
    It follows:
    \begin{IEEEeqnarray*}{0l}
      \limx{n} \frac{f^{-1}(b_n) - f^{-1}(w)}{b_n - w}
      = \limx{n} \frac{a_n - z}{f(a_n) - f(z)}
      = \frac{1}{f'(z)}
    \end{IEEEeqnarray*}
  \item Yes, it can be done under this additional assumption.
    Clearly, $\img(f)$ is open, so it makes sense to ask for complex differentiability in $w$.
    All we have to do is to prove that $f^{-1}$ is continuous in $w$.
    Define ${g \df \fn{\zeta}{\Om}{\frac{f(\zeta)}{f'(z)}}}$, then:
    \begin{IEEEeqnarray*}{0l}
      1 = g'(z) = \flim{\zeta}{z} \frac{g(\zeta) - g(z)}{\zeta - z}
    \end{IEEEeqnarray*}
    From this, we conclude that there is a neighborhood $N \subseteq \Om$ of $z$
    such that:
    \begin{IEEEeqnarray*}{0l}
      \forall \zeta \in N \holds \frac{\abs{g(\zeta) - g(z)}}{\abs{\zeta - z}} > \frac{1}{2}
    \end{IEEEeqnarray*}
    (For contradiction, assume that in each neighborhood of $z$ there was $\zeta$
    such that this does not hold.
    This yields a sequence $\seq{\zeta_{n}}{n}$
    tending to $z$ and for which that corresponding sequence of differential quotients
    cannot tend to $1$.)
    It follows:
    \begin{IEEEeqnarray*}{0l}
      \forall \zeta \in N \holds \frac{2}{f'(z)} \ccdot \abs{f(\zeta) - f(z)} > \abs{\zeta - z}
    \end{IEEEeqnarray*}
    Denote $M \df \fnimg{f}(N)$.
    Then, since $f$ is an open map, $M$ is a neighborhood of $f(z) = w$.
    Let $\seq{b_{n}}{n}$ be a sequence in $M$ with ${\limx{n} b_{n} = w}$.
    This sequence will eventually run in $M$,
    so there is $n_{0}$ such that for all $n \geq n_{0}$, we have $b_{n} \in M$
    and hence $f^{-1}(b_n) \in N$, so:
    \begin{IEEEeqnarray*}{0l}
      \abs{f^{-1}(b_n) - f^{-1}(w)}
      < \frac{2}{f'(z)} \ccdot \abs{f(f^{-1}(b_n)) - f(f^{-1}(w))}
      = \frac{2}{f'(z)} \ccdot \abs{b_n - z}
    \end{IEEEeqnarray*}
    Since $\limx{n} \abs{b_n - z} = 0$, it follows $\limx{n} \abs{f^{-1}(b_n) - f^{-1}(w)} = 0$,
    and hence $\limx{n} f^{-1}(b_n) = f^{-1}(w)$.
  \end{enumerate}
\end{solution}

%%% Local Variables:
%%% TeX-master: "Complex_Analysis_NatAppSci.tex"
%%% End:
